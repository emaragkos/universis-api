import request from 'supertest';
import {assert} from 'chai';
import app from '../app';
import passport from 'passport';
import {DataQueryable} from '@themost/data';
import {TestBearerStrategy} from '../utils';
import * as packageJson from './../../package';
import sinon from 'sinon';

const REGISTRAR_BEARER = new TestBearerStrategy({
    "name": "registrar1@example.com",
    "authenticationType":"Bearer",
    "authenticationToken": "test-api-server-token==",
    "authenticationScope": "registrar"
});

const STUDENTS_BEARER = new TestBearerStrategy({
    "name": "student1@example.com",
    "authenticationType":"Bearer",
    "authenticationToken": "test-api-server-token==",
    "authenticationScope": "students"
});

describe('Test DiagnosticsService', () => {

    before(done => {
        return done();
    });

    it('GET /api/diagnostics/services', async () => {
        passport.use(REGISTRAR_BEARER);
        // noinspection JSCheckFunctionSignatures
        let response = await request(app)
            .get('/api/diagnostics/services')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.status, 200);
    });

    it('GET /api/diagnostics/status with scope students', async () => {
        passport.use(STUDENTS_BEARER);
        // noinspection JSCheckFunctionSignatures
        let response = await request(app)
            .get('/api/diagnostics/status')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.status, 200);
    });

    it('GET /api/diagnostics/status', async () => {
        passport.use(REGISTRAR_BEARER);
        // stub DataQueryable.prototype.getItem
        const stub = sinon.stub(DataQueryable.prototype, 'getItem');
        stub.get(() => {
            return function() {
                return Promise.resolve({
                    databaseStatus: 'sync',
                    dateLastAttached: null,
                    dateModified: new Date()
                });
            }
        });
        let response = await request(app)
            .get('/api/diagnostics/status')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        stub.restore();
        assert.equal(response.status, 200);
        assert.isOk(response.body);
        assert.containsAllKeys(response.body, [ 'version', 'database','modifiedAt', 'attachedAt' ]);
        assert.isOk(response.body.version);
        assert.equal(response.body.version, packageJson.version);
        assert.isOk(response.body.database);

    });

    it('GET /api/diagnostics/status defaults', async () => {
        passport.use(REGISTRAR_BEARER);
        // stub DataQueryable.prototype.getItem
        const stub = sinon.stub(DataQueryable.prototype, 'getItem');
        stub.get(() => {
            return function() {
                return Promise.resolve();
            }
        });
        let response = await request(app)
            .get('/api/diagnostics/status')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        stub.restore();
        assert.equal(response.status, 200);
        assert.isOk(response.body);
        assert.containsAllKeys(response.body, [ 'version', 'database','modifiedAt', 'attachedAt' ]);
        assert.equal(response.body.database, 'online');

    });

    it('GET /api/diagnostics/status and get sync mode', async () => {
        passport.use(REGISTRAR_BEARER);
        // stub DataQueryable.prototype.getItem
        const stub = sinon.stub(DataQueryable.prototype, 'getItem');
        stub.get(() => {
            return function() {
                return Promise.resolve({
                    databaseStatus: 'sync',
                    dateLastAttached: null,
                    dateModified: new Date()
                });
            }
        });
        let response = await request(app)
            .get('/api/diagnostics/status')
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        stub.restore();
        assert.equal(response.status, 200);
        assert.isOk(response.body);
        assert.containsAllKeys(response.body, [ 'version', 'database','modifiedAt', 'attachedAt' ]);
        assert.equal(response.body.database, 'sync');
    });

});
