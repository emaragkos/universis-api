import {assert} from 'chai';
import app from '../app';
import {ExpressDataApplication} from '@themost/express';
// eslint-disable-next-line no-unused-vars
import {getMailer,MailHelper} from '@themost/mailer';
import TEST_STUDENT_REGISTRATION from './test-mailer-student-registration';
import path from 'path';
import moment from 'moment';
import TEST_STUDENT from './test-student';
import TEST_STUDENT_COURSES from './test-student-courses';
import TEST_STUDENT_COURSES_COMPLEX from './test-student-courses-complex';
import TEST_STUDENT_COURSES_COUNT from './test-student-courses-count'
import fs from 'fs';
import {RandomUtils} from "@themost/common/utils";
import pdf from 'html-pdf'
import async from 'async';
import util from 'util';

describe('Mailer', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    /**
     * @type MailHelper|*
     */
    let mailer;
    before(done => {
        // eslint-disable-next-line no-console
        console.log('INFO', 'IMPORTANT NOTE', 'Please install and start an SMTP development server from https://github.com/djfarrelly/MailDev to run this test');
        // set application configuration mail settings to local development mail server
        app.get(ExpressDataApplication.name).getConfiguration().setSourceAt('settings/mail', {
            port: 1025,
            ignoreTLS: true
        });
        context = app.get(ExpressDataApplication.name).createContext();
        mailer = getMailer(context);
       return done();
    });

    it('should send a text message', async () => {
        const mailer = getMailer(context);
        assert.isObject(mailer);
        await new Promise((resolve, reject) => {
            mailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Test Message')
                .text('Hello World!')
                .send(null, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
        });
    });

    it('should send an html message', async () => {
        assert.isObject(mailer);
        await new Promise((resolve, reject) => {
            mailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Test Message')
                .body('<h1>Hello World!</h1>')
                .send(null, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
        });
    });

    it('should send a template message', async () => {
        assert.isObject(mailer);
        await new Promise((resolve, reject) => {
            mailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Test Message')
                .template('test')
                .send({
                    message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
                }, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
        });
    });

    it('should send a student registration message', async () => {
        const newMailer = getMailer(context);
        // change template path toi test templates
        newMailer.getTemplatePath = function(template, extension) {
            return path.resolve(process.cwd(), `server/tests/templates/mails/${template}/html.${extension}`)
        };
        await new Promise((resolve, reject) => {
            newMailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Αποστολή Δήλωσης Μαθημάτων')
                .template('student-registration')
                .send(Object.assign(TEST_STUDENT_REGISTRATION, {
                    html: {
                        moment: moment
                    }
                }), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
        });
    });


    function sendMessages(app, subject, body, courseClass) {
        const context = app.createContext();
        let failed = [];
        let succeeded = [];
        (async function() {
            const newMailer = getMailer(context);
            // change template path toi test templates
            newMailer.getTemplatePath = function(template, extension) {
                return path.resolve(process.cwd(), `server/templates/mails/mail-after-message/html.${extension}`)
            };

            let testData = ['anthioikonomou@gmail.com'];
            for (let i = 0; i < 1000; i++) {
                testData.push ('test' + i + '@dispostable.com');
            }

            for (let i = 0; i < testData.length; i++) {
                let email = testData[i];
                console.log(email);
                try {
                    await new Promise((resolve, reject) => {
                        newMailer.from('test@example.com')
                            .subject(subject)
                            .body(body)
                            .to(email)
                            .send(null,(err) => {
                                if (err) {
                                    return reject(err);
                                }
                                return resolve();
                            });
                    });
                    succeeded.push(email);
                }
                catch(err) {
                    console.log(err);
                    failed.push(email);
                }
            }

        })().then(() => {
            context.finalize(()=> {
                console.log('DEBUG', 'FAILED', failed.length, 'SUCCEEDED', succeeded.length);
            });
        }).catch(err => {
            context.finalize(()=> {
                console.log('DEBUG', 'FAILED', failed.length, 'SUCCEEDED', succeeded.length);
            });
        });

    }

    it('should send mass emails',  (done) => {

        sendMessages(app.get(ExpressDataApplication.name), 'test', 'test message', '12345');
        setTimeout(()=> {
                console.log ('Done!');
                return done();
        }, 15000);

    });



    it('should create a student transcript pdf', async () => {
        const newMailer = getMailer(context);
        // change template path toi test templates
        newMailer.getTemplatePath = function(template, extension) {
            return path.resolve(process.cwd(), `server/templates/mails/${template}/html.${extension}`)
        }
        const testData ={
            student:TEST_STUDENT,
            courses:TEST_STUDENT_COURSES_COMPLEX
        }
        await new Promise((resolve, reject) => {
            newMailer.from('test@example.com')
                .template('draft-transcript')
                .test (true)
                .send(Object.assign(testData, {
                    html: {
                        moment: moment
                    }
                }), (err, data) => {
                    if (err) {
                        return reject(err);
                    }
                    if (newMailer._test) {
                        const self = this;
                        //generate pdf
                        let options = {
                            format: 'A4', "border": {
                                "top": "0.5in",
                                "right": "0.5in",
                                "bottom": "0.5in",
                                "left": "0.5in"
                            }
                        };
                        const fileName = RandomUtils.randomChars(12).toUpperCase() + '.pdf';
                        const tmpFile = path.resolve(process.cwd(), '.tmp/' + fileName);
                        //get random file name
                        pdf.create(data.html, options).toFile(tmpFile, function(err, res) {
                            return resolve();
                        });
                    }
                    return resolve();
                });
        });
    });

    it('should send a student transcript message', async () => {
        const newMailer = getMailer(context);
        // change template path toi test templates
        newMailer.getTemplatePath = function(template, extension) {
            return path.resolve(process.cwd(), `server/tests/templates/mails/${template}/html.${extension}`)
        }

        await new Promise((resolve, reject) => {
            newMailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Αποστολή  Πρόχειρης Βαθμολογίας')
                .template('student-transcript')
                .test (true)
                .send(Object.assign(TEST_STUDENT,{ courses: TEST_STUDENT_COURSES_COMPLEX }, {
                    html: {
                        moment: moment
                    }
                }), (err, data) => {
                    if (err) {
                        return reject(err);
                    }
                    if (newMailer._test) {
                        fs.writeFileSync(path.resolve(__dirname, './.tmp/student-transcript-message.html'),data.html, 'utf8');
                    }
                    return resolve();
                });
        });
    });

});
