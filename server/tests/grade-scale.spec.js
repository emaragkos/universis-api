import app from './test-app';
import {assert} from 'chai';
/**
 * @type {ExpressDataContext}
 */
let context;

let numericGradeScale = {
    "name": "0-10",
    "scaleType": 0,
    "scaleFactor": 0.1,
    "scaleBase": 0.5,
    "formatPrecision": 2,
    "scalePrecision": 4,
    "values": []
};

let literalGradeScale = {
    "name": "Letter Grades",
    "scaleType": 1,
    "scaleBase": 0.6,
    "formatPrecision": 4,
    "scalePrecision": 4,
    "values": [
        {
             "name": "A",
            "alternateName": "A",
            "valueFrom": 0.90,
            "valueTo": 1,
            "exactValue": 0.95
        },
        {
            "name": "B",
            "alternateName": "B",
            "valueFrom": 0.80,
            "valueTo": 0.8999,
            "exactValue": 0.85
        },
        {
            "name": "C",
            "alternateName": "C",
            "valueFrom": 0.70,
            "valueTo": 0.7999,
            "exactValue": 0.75
        },
        {
            "name": "D",
            "alternateName": "D",
            "valueFrom": 0.60,
            "valueTo": 0.6999,
            "exactValue": 0.65
        },
        {
            "id": 20000004,
            "name": "F",
            "alternateName": "F",
            "valueFrom": 0,
            "valueTo": 0.5999,
            "exactValue": 0.55
        }
    ]
};

let passedFailedGradeScale = {
    "name": "Passed Failed Grades",
    "scaleType": 3,
    "scaleBase": 0.5,
    "formatPrecision": 2,
    "scalePrecision": 4,
    "values": [
        {
            "name": "PASSED",
            "alternateName": "PASS",
            "valueFrom": 0.5,
            "valueTo": 1,
            "exactValue": 1
        },
        {
            "name": "FAILED",
            "alternateName": "FAIL",
            "valueFrom": 0,
            "valueTo": 0.4999,
            "exactValue": 0
        }
    ]
};

describe('Testing Universis Grade Scales', () => {

    before(function(done) {
        context = app.createContext();
        context.locale = "el";
        return done();
    });

    after(function(done) {
        context.finalize(()=> {
           return done();
        });
    });

    it('should use Number.prototype.toLocaleString', (done) => {
        let strNumber = new Intl.NumberFormat("el", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(6.7);
        assert.equal(strNumber,"6,70");
        return done();
    });

    it('should load grade scale and convert grade', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(numericGradeScale);
        let grade = gradeScale.convertFrom("7,65");
        assert.equal(grade, 0.765);
    });

    it('should load grade scale and format literal grade', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(literalGradeScale);
        // throw error
        assert.throws(()=> {
            gradeScale.convertFrom("U");
        });
        let grade = gradeScale.convertFrom("B");
        assert.equal(grade, 0.85);
        // format grade
        assert.equal(gradeScale.convertTo(0.87), "B");
    });

    it('should load grade scale and throw grade out of range error', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(literalGradeScale);
        assert.throws(()=> {
            gradeScale.convertFrom("17,6");
        });
    });

    it('should load grade scale and format grade', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(numericGradeScale);
        // format grade and hold digits
        assert.equal(gradeScale.convertTo(0, true), "0,00");
        // format grade without digits
        assert.equal(gradeScale.convertTo(1), "10");
    });

    it('should load grade scale (passed, failed) and format grade', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(passedFailedGradeScale);
        // format grade and hold digits
        assert.equal(gradeScale.convertTo(0), "FAILED");
        // format grade without digits
        assert.equal(gradeScale.convertTo(1), "PASSED");
    });
    it('should load grade scale (passed, failed) and format grade from grade file', async () => {
        /**
         * @type {GradeScale|*}
         */
        let gradeScale = context.model("GradeScale").convert(passedFailedGradeScale);
        // convert grade and hold value
        assert.equal(gradeScale.convertFrom("PASS"), 1);
        // convert grade and hold value
        assert.equal(gradeScale.convertFrom("FAIL"), 0);
        // expect Out of range value for grade
        assert.throws( () => {
            gradeScale.convertFrom(1)
        }, "Out of range value for grade");
    });
    it('should load grade scales and return null when null or undefined is passed', async () => {
        /**
         * @type {GradeScale|*}
         */
        let passFailScale = context.model("GradeScale").convert(passedFailedGradeScale);
        // convert grade and hold value
        assert.equal(passFailScale.convertFrom(null), undefined);
        // convert grade and hold value
        assert.equal(passFailScale.convertFrom(undefined), undefined);
        let numericScale = context.model("GradeScale").convert(numericGradeScale);
        assert.equal(numericScale.convertFrom(null), undefined);
        // convert grade and hold value
        assert.equal(numericScale.convertFrom(undefined), undefined);
        let literalScale = context.model("GradeScale").convert(literalGradeScale);
        assert.equal(literalScale.convertFrom(null), undefined);
        // convert grade and hold value
        assert.equal(literalScale.convertFrom(undefined), undefined);
    });
});
