import {ApplicationService} from "@themost/common";
import {TraceUtils} from "@themost/common";
import {DataConfigurationStrategy} from '@themost/data';
import path from 'path';

export class ApplicationServiceAsListener extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
    }

    install(targetModel, listener) {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition(targetModel);
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), listener);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info(`Services: ${this.constructor.name} service has been successfully installed`);
        }
    }

    uninstall(targetModel, listener) {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition(targetModel);
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), listener);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.find( listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex>=0) {
            // remove event listener
            model.eventListeners.splice(findIndex, 1);
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info(`Services: ${this.constructor.name} service has been successfully uninstalled`);
        }
    }


}