import unirest from 'unirest';
import {URL} from 'url';
import {ApplicationService, HttpError, DataError} from '@themost/common';
import {InvalidMessageRecipient} from '@universis/messages';

class SmsGatewayCatcherService extends ApplicationService {
    constructor(app) {
        super(app);

        this.settings = Object.assign({
            "sender": "Universis",
            "url": "http://localhost:3030",
            "user": "developer",
            "password": "secret"
        }, app.getConfiguration().getSourceAt("settings/sms"));
    }

    async sendSMS(context, message) {
        let recipient = message.recipient;
		const smsRecipient = await context.model('CandidateStudent')
			.where('user').equal(recipient)
			.select('person/mobilePhone as phone').silent()
			.getItem() ?? await context.model('Student')
			.where('user').equal(recipient)
			.select('person/mobilePhone as phone').silent()
			.getItem() ?? await context.model('Instructor')
			.where('user').equal(recipient)
			.select('workPhone as phone').silent()
			.getItem();
        
        if (smsRecipient == null) {
            throw new InvalidMessageRecipient('Message recipient cannot be found or is inaccessible'); 
        }
        if (smsRecipient.phone == null) {
            throw new InvalidMessageRecipient('Recipient phone cannot be found or is inaccessible');
        }

        return new Promise((resolve, reject) => {
            return unirest.post(new URL('api/messages/send', this.settings.url))
                .header('Authorization', 'Basic ' + Buffer.from(`${this.settings.user}:${this.settings.password}`).toString('base64'))
                .header('Accept', 'application/json')
                .header('Content-Type', 'application/json')
                .send({
                    sender: this.settings.sender,
                    recipient: smsRecipient.phone,
                    text: message.body
                }).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    Object.assign(message, {
                        identifier: response.body.identifier
                    });
                    return resolve(response.body);
                });
        });
    }

    async checkDelivery(message) {
        return new Promise((resolve, reject) => {
            return unirest.get(new URL(`api/messages/${message.identifier}`, this.settings.url))
                .header('Accept', 'application/json')
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
    }

    async checkBalance() {
		return new Promise((resolve, reject) => {
            return unirest.get(new URL(`api/messages/balance`, this.settings.url))
                .header('Accept', 'application/json')
                .end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    return resolve(response.body);
                });
        });
	}

}

export {
    SmsGatewayCatcherService
};