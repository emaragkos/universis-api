import {DefaultDataContext} from "@themost/data";
import i18n from "i18n";

const localeProperty = Symbol('locale');

/**
 * @name DefaultDataContext~__
 * @param {...*} any
 * @returns {string}
 */

/**
 * Implements i18n.__ method to DefaultDataContext
 * @returns {string}
 */
DefaultDataContext.prototype.__  = function() {
    return i18n.__.apply(this, arguments);
};

/**
 * @name DefaultDataContext~translate
 * @param {...*} any
 * @returns {string}
 */

/**
 * @returns {string}
 */
DefaultDataContext.prototype.translate  = function() {
    return i18n.__.apply(this, arguments);
};

/**
 * @name DefaultDataContext~__n
 * @param {...*} any
 * @returns {string}
 */

/**
 * Implements i18n.__n method to DefaultDataContext
 * @returns {string}
 */
DefaultDataContext.prototype.__n  = function() {
    return i18n.__n.apply(this, arguments);
};
/**
 * Gets or sets the locale of this data context
 * @name DefaultDataContext#locale
 * @type {string}
 */
Object.defineProperty(DefaultDataContext.prototype, 'locale', {
   get: function() {
       if (this[localeProperty]) {
           return this[localeProperty];
       }
       if (this.req && this.req.locale) {
           return this.req.locale;
       }
   },
   set: function(value) {
       this[localeProperty] = value;
   }
});


