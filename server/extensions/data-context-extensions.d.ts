
declare interface DefaultDataContextExtensions {
    locale?: string;
    __(...args : any[]): string;
    __n(...args : any[]): string;
}