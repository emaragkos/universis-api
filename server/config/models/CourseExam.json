{
  "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
  "@id": "https://universis.io/schemas/CourseExam",
  "name": "CourseExam",
  "title": "Εξέταση μαθήματος",
  "description": "Εξέταση συγκεκριμένου μαθήματος σε συγκεκριμένο έτος και εξεταστική περίοδο με φοιτητές μίας ή περισσοτέρων τάξεων",
  "hidden": false,
  "sealed": false,
  "version": "0.2",
  "classPath": "./models/course-exam-model",
  "source": "CourseExamBase",
  "view": "CourseExamData",
  "category": "main",
  "fields": [
    {
      "name": "id",
      "title": "Κωδικός",
      "description": "Ο κωδικός της εξέτασης",
      "type": "Integer",
      "nullable": false,
      "primary": true,
      "size": 4,
      "value": "javascript:return this.newid();",
      "readonly": true
    },
    {
      "name": "course",
      "title": "Μάθημα",
      "description": "Το μάθημα στο οποίο αναφέρεται η εξέταση.",
      "type": "Course",
      "nullable": false,
      "editable": false
    },
    {
      "name": "year",
      "title": "Ακαδημαϊκό έτος",
      "description": "Το ακαδημαϊκό έτος της εξέτασης",
      "type": "AcademicYear",
      "nullable": false,
      "editable": false,
      "expandable": true
    },
    {
      "name": "examPeriod",
      "title": "Εξεταστική περίοδος",
      "description": "Η εξεταστική περίοδος της εξέτασης",
      "type": "ExamPeriod",
      "editable": false,
      "nullable": false
    },
    {
      "name": "description",
      "title": "Περιγραφή",
      "description": "Περιγραφή της εξέτασης",
      "type": "Text",
      "nullable": true,
      "size": 80
    },
    {
      "name": "notes",
      "title": "Σημειώσεις",
      "description": "Σημειώσεις",
      "type": "Text",
      "nullable": true,
      "size": 100,
      "many": false
    },
    {
      "name": "isLate",
      "title": "Επαναληπτική",
      "description": "Αν είναι επαναληπτική η εξέταση (ΝΑΙ/ΟΧΙ)",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isLate');"
    },
    {
      "name": "examDate",
      "title": "Ημερομηνία εξέτασης",
      "description": "Η ημερομηνία διεξαγωγής της εξέτασης",
      "type": "DateTime"
    },
    {
      "name": "status",
      "title": "Κατάσταση",
      "description": "Η κατάσταση της εξέτασης.",
      "type": "CourseExamStatus",
      "nullable": false,
      "mapping": {
        "parentModel": "CourseExamStatus",
        "childModel": "CourseExam",
        "parentField": "identifier",
        "childField": "status",
        "associationType": "association"
      },
      "value": "javascript:return { alternateName:'open' };"
    },
    {
      "name": "isCalculated",
      "title": "Υπολογιζόμενη",
      "description": "Αν ο τελικός βαθμός της εξέτασης προκύπτει από επιμέρους βαθμούς (πχ πρόοδοι κλπ)",
      "type": "Boolean",
      "calculation": "javascript:return this.convertStrictBoolean(this.target,'isCalculated');"
    },
    {
      "name": "decimalDigits",
      "title": "Δεκαδικά",
      "description": "Αριθμός δεκαδικών ψηφίων του βαθμού.",
      "type": "Short",
      "many": false,
      "nullable": true,
      "value": "javascript:return 1;"
    },
    {
      "name": "name",
      "title": "Ονομασία",
      "description": "Η ονομασία της εξέτασης του μαθήματος (μπορεί να διαφέρει ανά έτος).",
      "type": "Text",
      "nullable": true,
      "size": 150
    },
    {
      "name": "resultDate",
      "title": "Ημερομηνία αποτελεσμάτων",
      "description": "Η ημερομηνία ανακοίνωσης των αποτελεσμάτων.",
      "type": "DateTime"
    },
    {
      "name": "numberOfGradedStudents",
      "title": "Αριθμός βαθμολογημένων φοιτητών",
      "description": "Ο αριθμός των φοιτητών που προσήλθαν στην εξέταση και βαθμολογήθηκαν.",
      "type": "Integer"
    },
    {
      "name": "completedByUser",
      "title": "Ολοκληρώθηκε από",
      "description": "Ο χρήστης που ολοκλήρωσε την εξέταση.",
      "type": "User"
    },
    {
      "name": "dateCompleted",
      "title": "Ημερομηνία ολοκλήρωσης",
      "description": "Η ημερομηνία ολοκλήρωσης της κατάθεσης της βαθμολογίας της εξέτασης.",
      "type": "DateTime"
    },
    {
      "name": "dateModified",
      "title": "Ημερομηνία τροποποίησης",
      "description": "Ημερομηνία τροποποίησης",
      "type": "DateTime",
      "value": "javascript:return new Date();",
      "calculation": "javascript:return this.now();"
    },
    {
      "name": "gradeScale",
      "title": "Κλίμακα βαθμολογίας",
      "description": "Η κλίμακα βαθμολογίας της εξέτασης.",
      "type": "GradeScale",
      "readonly": true
    },
    {
      "name": "classes",
      "title": "Τάξεις εξέτασης",
      "description": "Το σύνολο των τάξεων που μετέχουν στην εξέταση (πχ σε εξέταση Σεπτεμβρίου μπορεί να μετέχει και η χειμερινή αλλά και η εαρινή τάξη του μαθήματος).",
      "type": "CourseClass",
      "many": true,
      "mapping": {
        "parentModel": "CourseExam",
        "childModel": "CourseExamClass",
        "parentField": "id",
        "childField": "courseExam",
        "cascade": "delete",
        "associationType": "association"
      }
    },
    {
      "name": "instructors",
      "title": "Εξεταστές",
      "description": "Το σύνολο των εξεταστών της εξέτασης.",
      "type": "CourseExamInstructor",
      "many": true,
      "mapping": {
        "parentModel": "CourseExam",
        "childModel": "CourseExamInstructor",
        "parentField": "id",
        "childField": "courseExam",
        "cascade": "delete",
        "associationType": "association"
      }
    },
    {
      "name": "types",
      "title": "Course exam types",
      "description": "A collection of possible exam types.",
      "type": "TestType",
      "mapping": {
        "associationAdapter": "CourseExamTestTypes",
        "parentModel": "CourseExam",
        "parentField": "id",
        "childModel": "TestType",
        "childField": "id",
        "associationType": "junction",
        "associationObjectField": "courseExam",
        "associationValueField": "testType",
        "cascade": "delete",
        "select": [
          "id",
          "name",
          "alternateName"
        ]
      }
    }
  ],
  "constraints": [
    {
      "type": "unique",
      "description": "A course exam must be unique for an exam period and year.",
      "fields": [
        "course",
        "examPeriod",
        "year"
      ]
    }
  ],
  "privileges": [
    {
      "mask": 15,
      "type": "global"
    },
    {
      "mask": 1,
      "type": "global",
      "account": "Students"
    },
    {
      "mask": 15,
      "type": "global",
      "account": "Administrators"
    },
    {
      "mask": 16,
      "type": "global"
    },
    {
      "mask": 5,
      "type": "self",
      "filter": "instructors/instructor eq instructor()",
      "account": "Instructors"
    },
    {
      "mask": 16,
      "type": "self",
      "filter": "instructors/instructor eq instructor()",
      "account": "Instructors"
    },
    {
      "mask": 15,
      "type": "self",
      "account": "Registrar",
      "filter": "course/department eq departments()"
    }
  ],
  "eventListeners": [
    {
      "type": "./listeners/course-exam-listener",
      "name": "Course exam listener"
    }
  ]
}
