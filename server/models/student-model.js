import util from 'util';
import _ from 'lodash';
import { QueryEntity } from '@themost/query/query';
import {EdmMapping,EdmType} from '@themost/data/odata';
import {HttpServerError, HttpNotFoundError, HttpError,
    HttpForbiddenError, HttpBadRequestError, HttpConflictError} from '@themost/common/errors';
import {TraceUtils} from '@themost/common/utils';
import {DataObject} from "@themost/data/data-object";
import ActionStatusType from "../models/action-status-type-model";
import {DataConflictError, ValidationResult} from "../errors";
import moment from 'moment';
import {Args, DataError } from '@themost/common';

import * as esprima from 'esprima';
import {round} from "mathjs";
import {NumberFormatter} from "@universis/number-format";
import {promisify} from "es6-promisify";
import {DataObjectState, DataPermissionEventListener} from "@themost/data";
import EnableAttachmentModel from "./enable-attachment-model";
const Rule = require('./rule-model');
import fs from 'fs';

@EdmMapping.entityType("Student")
/**
 * @class
 */
class Student extends EnableAttachmentModel {
    constructor() {
        super();
        const self = this;
        self.selector('active',function(callback) {
            try {
                self.attrOf('studentStatus', function(err, result) {
                    if (err) { return callback(err); }
                    const status = self.context.model('StudentStatus').convert(result).getId();
                    callback(null, (status===1));
                });

            }
            catch(er) {
                callback(er);
            }
        });
        self.selector('me',function(callback) {
            try {
                let userName = self.context && self.context.user && self.context.user.name;
                if (self.context && self.context.interactiveUser) {
                    userName = self.context.interactiveUser.name;
                }
                self.context.model('Student').where('id').equal(self.getId())
                    .and('user/name').equal(userName).silent().count().then((result) => {
                        return callback(null, (result === 1));
                }).catch((err) => {
                    return callback(err);
                });
            }
            catch(er) {
                callback(er);
            }
        });
    }

    programOf(callback) {
        this.attrOf('studyProgram', callback);
    }

    departmentOf(callback) {
        this.attrOf('department', callback);
    }

    inferSemester(year, period, callback) {
        return this.inferSemesterAsync(year, period).then(function(result) {
            return callback(null, result);
        }).catch(function(err) {
            return callback(err);
        });
    }

    /**
     * @param {number} year
     * @param {number} period
     * @returns {Promise<number>}
     */
    async inferSemesterAsync(year, period) {

        /**
         * @type {Student}
         */
        const student = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('id', 'semester', 'inscriptionYear', 'inscriptionPeriod', 'inscriptionSemester', 'studentStatus/alternateName as studentStatus')
            .flatten()
            .getItem();
        let semester = student.semester;
        if (student.studentStatus === 'active') {
            // get last suspension if any
            const suspension = await this.context.model('StudentSuspension')
                .where('student').equal(this.id)
                .orderByDescending('reintegrationYear')
                .thenByDescending('reintegrationPeriod')
                .where('reintegrated').equal(true)
                .flatten()
                .getItem();
            if (suspension) {
                if (suspension.reintegrated) {
                    semester = (year - suspension.reintegrationYear) * 2 + (period - suspension.reintegrationPeriod) + suspension.reintegrationSemester;
                }
            } else {
                semester = (year - student.inscriptionYear) * 2 + (period - student.inscriptionPeriod) + student.inscriptionSemester;
            }
        }
        if (semester < 1) {
            semester = student.semester;
        }
        return semester;
    }

    

    getPowersOfTwo(value) {
        let b = 1;
        let res = [];
        while (b <= value) {
            if (b & value) {
                res.push(this.getPower(2, b));
            }
            b <<= 1;
        }
        return res;
    }
    getPower(x, y) {
        // Repeatedly compute power of x
        let pow = 1;
        let power = 0;
        while (pow < y) {
            pow = pow * x;
            power += 1;
        }
        // Check if power of x becomes y
        if (pow === y)
            return power;
    }

    /**
     * Returns a data queryable for the current student
     * @param {ExpressDataContext} context
     * @returns {DataQueryable}
     */
    @EdmMapping.func("Me", "Student")
    static getMe(context) {
        return context.model('Student').where('user/name').notEqual(null)
            .and('user/name').equal(context.user.name)
            .prepare();
    }

    /**
     *
     * @param {ExpressDataContext} context
     * @return {DataQueryable}
     */
    @EdmMapping.func("Active",EdmType.CollectionOf("Student"))
    static getActiveStudents(context) {
        return context.model('Student').where('studentStatus').equal(1).prepare();
    }


    /**
     *
     */
    @EdmMapping.func("LastPeriodRegistration","StudentPeriodRegistration")
    getLastPeriodRegistration() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).orderByDescending("registrationYear").thenByDescending("registrationPeriod").prepare();
    }

    /**
     *
     */
    @EdmMapping.func("Registrations",EdmType.CollectionOf("StudentPeriodRegistration"))
    getRegistrations() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).prepare();
    }

    /**
     *
     */
    @EdmMapping.param("data", "StudentPeriodRegistration", false, true)
    @EdmMapping.action("Registrations","StudentPeriodRegistration")
    async commitPeriodRegistration(data) {
        let self = this;
        // get context
        let context = self.context;
        // convert registration to data object
        let registration = context.model("StudentPeriodRegistration").convert(data);
        try {
            await registration.saveOne(context);
            return registration;
        } catch (err) {
            TraceUtils.error(err);
            throw (err);
        }
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
     @EdmMapping.param('registrationPeriod',EdmType.EdmInt32,false,false)
     @EdmMapping.param('registrationYear',EdmType.EdmInt32,false,false)
     @EdmMapping.func('periodAvailableClasses',EdmType.CollectionOf('StudentAvailableClass'))
     getAvailableClassesPerPeriod(registrationYear, registrationPeriod) {
         const self = this;
         const student = this.getId();
         return self.context.model('Student').convert(student).is(':me').then(isMe => {
             if (isMe) {
                 return Promise.reject(new HttpForbiddenError());
             }
             const entityExpr = util.format('ufnStudentAvailableClasses(%s,%s,%s)', registrationYear, registrationPeriod, student);
             const entity = new QueryEntity(entityExpr).as('StudentAvailableClasses');
             const q = self.context.model('StudentAvailableClass').asQueryable();
             //replace entity reference
             q.query.from(entity);
             //return queryable
             return Promise.resolve(q);
         }).catch(function (err) {
             TraceUtils.error(err);
             if (err instanceof HttpError) {
                 return Promise.reject(err);
             }
             return Promise.reject(new HttpServerError());
         });
     }

    /**
     *
     */
    @EdmMapping.func("Suspensions",EdmType.CollectionOf("StudentSuspension"))
    getSuspensions() {
        return this.context.model('StudentSuspension').where("student").equal(this.getId()).prepare();
    }

    /**
     *
     */
    @EdmMapping.func("Courses",EdmType.CollectionOf("StudentCourse"))
    getCourses() {
        return this.context.model('StudentCourse').where("student").equal(this.getId()).prepare();
    }
    /**
     *
     */
    @EdmMapping.func("GraduationRules", ValidationResult)
    async checkGraduationRules(name) {
        const context = this.context;
        const student = await context.model('Student').where("id").equal(this.getId()).getTypedItem();
        const data = {
            student: student
        };

        // check if student has personal graduation rules
        let additionalType = 'StudentGraduateRule';
        let graduationRules = await context.model('Rule').where('additionalType').equal('GraduateRule')
            .and('targetType').equal('Student')
            .and('target').equal(student.id.toString())
            .getAllItems();

        // get program specialty graduation rules
        if (graduationRules && graduationRules.length === 0) {
            additionalType ='ProgramGraduateRule';
            graduationRules = await context.model('Rule').where('additionalType').equal('GraduateRule')
                .and('targetType').equal('Program')
                .and('target').equal(student.studyProgram.toString())
                .and('programSpecialty').equal(student.specialtyId)
                .getAllItems();

        }
        if (graduationRules && graduationRules.length === 0) {
            return new ValidationResult(false, 'FAIL', 'Graduation rules have not been set.');
        } else {
            let validationResults = [];
            // add async function for validating graduation rules
            let forEachRule = (graduationRule) => {
                try {
                    return new Promise((resolve) => {
                        const ruleModel = context.model(graduationRule.refersTo + 'Rule');
                        if (_.isNil(ruleModel)) {
                            validationResults = validationResults || [];
                            const errorResult = new ValidationResult(false, 'FAIL',  context.__('Student validation rule type cannot be found.'));
                            validationResults.push(errorResult);
                            graduationRule.validationResult=errorResult;
                            return resolve();
                        }
                        const rule = ruleModel.convert(graduationRule);
                        rule.validate(data, function (err, result) {
                            if (err) {
                                validationResults = validationResults || [];
                                const errorResult = new ValidationResult(false, 'FAIL', err.message);
                                validationResults.push(errorResult);
                                graduationRule.validationResult=errorResult;
                                return resolve();
                            }
                            /**
                             * @type {ValidationResult[]}
                             */
                            validationResults = validationResults || [];
                            validationResults.push(result);
                            graduationRule.validationResult = result;
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            };
            // call all promises

            for (let graduationRule of graduationRules) {
                await forEachRule(graduationRule);
            }
            const fnValidateExpression = function (x) {
                try {
                    let expr = x['ruleExpression'];
                    if (_.isEmpty(expr)) {
                        return false;
                    }
                    expr = expr.replace(/\[%(\d+)]/g, function () {
                        if (arguments.length === 0) return;
                        const id = parseInt(arguments[1]);
                        const v = validationResults.find(function (y) {
                            return y.id === id;
                        });
                        if (v) {
                            return v.success.toString();
                        }
                        return 'false';
                    });
                    expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                    return eval(expr);
                } catch (e) {
                    return e;
                }
            };

            const fnTitleExpression = function (x) {
                try {
                    let expr = x['ruleExpression'];
                    if (_.isEmpty(expr)) {
                        return false;
                    }
                    expr = expr.replace(/\[%(\d+)]/g, function () {
                        if (arguments.length === 0) return;
                        const id = parseInt(arguments[1]);
                        const v = validationResults.find(function (y) {
                            return y.id === id;
                        });
                        if (v) {
                            return '(' + v.message.toString() + ')';
                        }
                        return 'Unknown Rule';
                    });
                    expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                    return expr;
                } catch (e) {
                    return e;
                }
            };

            let res, title;

            //apply default expression
            const expr = graduationRules.find(function (x) {
                return !_.isEmpty(x['ruleExpression']);
            });
            let finalResult;
            if (expr) {
                res = fnValidateExpression(expr);
                title = fnTitleExpression(expr);
                finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                finalResult.type = 'GraduationRule';
                let ruleExpression = expr.ruleExpression;
                ruleExpression= ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                ruleExpression = ruleExpression.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    return parseInt(arguments[1]);
                });
                try {
                    finalResult.expression= esprima.parse(ruleExpression);
                }
                catch (e) {
                    //
                }
            } else {
                //get expression (for this rule type)
                const ruleExp1 = graduationRules.map(function (x) {
                    return '[%' + x.id + ']';
                }).join(' AND ');
                res = fnValidateExpression({ruleExpression: ruleExp1});
                title = fnTitleExpression({ruleExpression: ruleExp1});
                finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                finalResult.type = 'GraduationRule';
                finalResult.expression = null;
            }
            // set additionalType
            finalResult.additionalType = additionalType;

            let response = {finalResult, "graduationRules": graduationRules}
            return response;
        }
    }

    /**
     *
     */
    @EdmMapping.func("availableExamsForParticipation", EdmType.CollectionOf("CourseExam"))
    async getAvailableExamsForParticipation() {
        const context = this.context;
        const student = await context.model('Student').where("id").equal(this.getId()).getTypedItem();
        const data = {
            student: student
        };
        // check if student is active
        const active = await this.isActive();
        // if student is not active
        if (!active) {
            // throw error
            return new ValidationResult(false, 'ESTATUS', 'Student is not active');
        }

        // get current date
        const currentDate = moment(new Date()).startOf('day').toDate(); // get only date format

        const department = await context.model('DepartmentConfiguration').where('department').equal(student.department)
            .and('date(examParticipationPeriodStart)').lowerOrEqual(currentDate)
            .and('date(examParticipationPeriodEnd)').greaterOrEqual(currentDate)
            .select('id', 'name', 'examParticipationPeriodStart', 'examParticipationPeriodEnd',
                'examYear', 'examPeriod')
            .flatten()
            .silent()
            .getItem();
        // if department is null then return status for closed registration period
        if (department != null) {

            const year = department.examYear;
            const examPeriod = department.examPeriod;
            if (year && examPeriod) {
                // get student course classes
                let courseClasses = await context.model('StudentCourseClass').where('courseClass/year').equal(year)
                    .and('student').equal(student.getId())
                    .expand('courseClass')
                    .orderBy('courseClass/course')
                    .thenBy('courseClass/year').thenByDescending('courseClass/period')
                    .getAllItems();

                let availableCourseExams = []

                if (courseClasses && courseClasses.length > 0) {
                    // get last registered class (distinct)
                    let lastCourseClasses = [];
                    courseClasses.forEach(courseClass => {
                        const found = lastCourseClasses.find((x) => {
                            return x.courseClass.course === courseClass.courseClass.course && x.courseClass.period > courseClass.courseClass.period;
                        });
                        if (!found) {
                            lastCourseClasses.push(courseClass);
                        }
                    });
                    // get course classes
                    let values = lastCourseClasses.filter(x => {
                        return x.isPassed === 0
                    }).map(x => {
                        return x.courseClass.id;
                    });
                    //get all available exams related to student classes
                    let courseExams = await context.model('CourseExam').where('year').equal(year)
                        .and('examPeriod').equal(examPeriod)
                        .and('classes/courseClass').in(values)
                        .expand('course','year','examPeriod','types')
                        .silent()
                        .getAllItems();

                    // remove duplicates exams and exams with passed grade
                    courseExams.forEach(courseExam => {
                        const found = availableCourseExams.find((x) => {
                            return x.id === courseExam.id;
                        });
                        if (!found) {
                            availableCourseExams.push(courseExam);
                        }
                    });
                    return  availableCourseExams;
                }
            }
        }
    }

        /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("Requests",EdmType.CollectionOf("RequestAction"))
    getRequests() {
        return new Promise((resolve, reject) => {
            return this.context.model('RequestAction').filter({
                $filter: "owner eq me()"
            }, (err, query)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(query);
            });
        });
    }

    /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("availableGraduationEvents",EdmType.CollectionOf("GraduationEvent"))
    async getAvailableGraduationEvents() {
        // get department
        const department = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('studyProgram','department/id as id', 'department/currentYear as currentYear', 'department/currentPeriod as currentPeriod','semester as studentSemester')
            .getItem();

        // get opened events for department current year and period
        const currentDate = moment(new Date()).startOf('day').toDate(); // get only date format
        return this.context.model('GraduationEvent')
            .where("organizer").equal(department.id)
            .and("graduationYear").equal(department.currentYear)
            .and("graduationPeriod").equal(department.currentPeriod)
            .and('date(validFrom)').lowerOrEqual(currentDate)
            .and('date(validThrough)').greaterOrEqual(currentDate)
            .and('eventStatus/alternateName').equal('EventOpened')
            .and('studyPrograms/id').equal(department.studyProgram)
            .and('studyPrograms/semesters').lowerOrEqual(department.studentSemester)
            .prepare();
    }

    /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("Messages",EdmType.CollectionOf("StudentMessage"))
    getMessages() {
        return new Promise((resolve, reject) => {
            return this.context.model('StudentMessage').filter({
                $filter: "student eq " + this.getId()
            }, (err, query) => {
                if (err) {
                    return reject(err);
                }
                return resolve(query);
            });
        });
    }

    /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("graduationRequests",EdmType.CollectionOf("GraduationRequestAction"))
    getGraduationRequests() {
        return new Promise((resolve, reject) => {
            return this.context.model('GraduationRequestAction').filter({
                $filter: "student eq " + this.getId()
            }, (err, query) => {
                if (err) {
                    return reject(err);
                }
                return resolve(query);
            });
        });
    }
    /**
     *
     */
    @EdmMapping.func("Grades",EdmType.CollectionOf("StudentGrade"))
    getGrades() {
        const self = this;
        const context=self.context;
        // get student department organization configuration
        return self.getModel().select('id', 'department').expand({
                'name':'department',
                'options':
                    {'$expand':'organization($expand=instituteConfiguration)'

                    }
            }).where('id').equal(self.id).getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            // get showPendingGrades setting
            const showPendingGrades =
                result.department &&
                result.department.organization &&
                result.department.organization.instituteConfiguration &&
                result.department.organization.instituteConfiguration.showPendingGrades;
            if (showPendingGrades) {
                return context.model('StudentGrade').where("student").equal(self.getId()).prepare();
            } else {
                return context.model('StudentGrade').where("student").equal(self.getId()).and('status/alternateName').notEqual('pending').prepare();
            }
        });
    }
    /**
     *
     */
    @EdmMapping.func("Classes",EdmType.CollectionOf("StudentCourseClass"))
    getClasses() {
        return this.context.model('StudentCourseClass').where("student").equal(this.getId()).prepare();
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func("currentRegistration","StudentPeriodRegistration")
    async getCurrentRegistration() {
        const department = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .getItem();
        return this.context.model('StudentPeriodRegistration')
            .where("student").equal(this.id)
            .and("registrationYear").equal(department.currentYear)
            .and("registrationPeriod").equal(department.currentPeriod)
            .prepare();
    }

    /**
     *
     */
    @EdmMapping.param("data", "StudentPeriodRegistration", false, true)
    @EdmMapping.action("currentRegistration","StudentPeriodRegistration")
    commitCurrentRegistration(data) {
        let self = this;
        // get context
        let context = self.context;
        // convert registration to data object
        let registration = context.model("StudentPeriodRegistration").convert(data);
        return new Promise((resolve, reject) => {
            registration.is(':current').then(function (result) {
                if (result) {
                    //execute in unattended mode
                    context.unattended(function (cb) {
                            //do save registration
                            registration.save(context, function (err) {
                                cb(err);
                            });
                        }, function (err) {
                            if (err) {
                                TraceUtils.error(err);
                                return reject(new HttpServerError());
                            }
                            return resolve(registration);
                        });
                }
                else {
                    return reject(new HttpForbiddenError('Invalid registration data. The specified registration is not referred to current academic year and period.'));
                }
            }, function (err) {
                TraceUtils.error(err);
                return reject(new HttpServerError());
            });
        });

    }

    /**
     * @returns {Promise<*>}
     */
    @EdmMapping.func('currentRegistrationStatus', 'PeriodRegistrationEffectiveStatus')
    async getCurrentRegistrationStatus() {
        const self = this;
        // get period effective status types
        const statusTypes = await this.context.model('PeriodRegistrationEffectiveStatus').getItems();
        const studentStatus = await this.property('studentStatus').getItem();
        if (studentStatus.alternateName !== 'active') {
            return statusTypes.find(function (x) {
                return x.code === 'INACTIVE_STUDENT'
            });
        }
        const online = await self.context.model('Workspace').where('databaseStatus').equal('online').count();
        if (!online) {
            return {
                "status": "closed",
                "code": "P_SYSTEM_REPLICATING"
            };
        }
        // validate department registration period
        const studentDepartment = await this.property('department').select('id').value();
        // get current date
        const fromDate = new Date(); // get current date with time
        const toDate = moment(new Date()).startOf('day').toDate(); // get only date format
        const department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
            .and('date(registrationPeriodStart)').lowerOrEqual(fromDate)
            .and('date(registrationPeriodEnd)').greaterOrEqual(toDate)
            .select('id', 'name', 'registrationPeriodStart', 'registrationPeriodEnd', 'currentYear', 'currentPeriod')
            .flatten()
            .silent()
            .getItem();
        // if department is null then return status for closed registration period
        if (department == null) {
            return statusTypes.find(function (x) {
                    return x.code === 'CLOSED_REGISTRATION_PERIOD';
                });
        }

        // get current registration
        const registration = await this.context.model('StudentPeriodRegistration')
            .where("student").equal(this.getId()).and("registrationYear")
            .equal(department.currentYear)
            .and("registrationPeriod").equal(department.currentPeriod)
            .expand('classes').getItem();
        // if student does not have a period registration for current academic period

        //check registration status (closed)
        if (registration && registration.status.alternateName === 'closed') {
            //registration exists and is closed
            return statusTypes.find(function (x) {
                return x.code === 'CLOSED_REGISTRATION';
            });
        }
        // check if student can (or should) select specialty
        const canSelectSpecialty = await this.canSelectSpecialty();
        // if result is success
        if (canSelectSpecialty.success === true) {
            // return status for select specialty
            return statusTypes.find(function (x) {
                return x.code === 'SELECT_SPECIALTY';
            });
        }

        if (registration == null) {
            // return result for open with no transaction
            return statusTypes.find(function (x) {
                    return x.code === 'OPEN_NO_TRANSACTION'
                });
        }

        // get last document
        const lastDocument = await this.context.model('StudentRegistrationDocument')
            .select('id','documentStatus')
            .where("registration").equal(registration.id)
            .orderByDescending('dateCreated')
            .getItem();
        // if there is no registration document at all
        if (lastDocument == null) {
            // if registration does not
            if (registration.classes.length > 0) {
                //secretary has registered classes
                return statusTypes.find(function (x) {
                    return x.code === 'P_SYSTEM_TRANSACTION';
                });
            }
            return statusTypes.find(function (x) {
                    return x.code === 'OPEN_NO_TRANSACTION'
                });
        }
        // check last document status
        switch (lastDocument.documentStatus.alternateName) {
            case 'pending':
                return statusTypes.find(function (x) {
                        return x.code === 'PENDING_TRANSACTION';
                    });
            case 'closed':
                return statusTypes.find(function (x) {
                        return x.code === 'SUCCEEDED_TRANSACTION'
                    });
            default:
                return statusTypes.find(function (x) {
                    return x.code === 'FAILED_TRANSACTION'
                });
        }
    }


    /**
     * @return {Promise<DataQueryable>}
     */
    @EdmMapping.func("availableSpecialties",EdmType.CollectionOf("StudyProgramSpecialty"))
    getAvailableSpecialties() {
        const self = this;
        return self.getModel().select('id', 'studyProgram').where('id').equal(self.id).flatten().getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            return self.context.model('StudyProgramSpecialty').where("studyProgram").equal(result.studyProgram).prepare();
        });
    }

    /**
     * @return {Promise<DataQueryable>}
     */
    @EdmMapping.func("availableCourses",EdmType.CollectionOf("SpecializationCourse"))
    getAvailableCourses() {
        const self = this;
        // get student
        return self.getModel().select('id', 'studyProgram', 'studyProgramSpecialty/specialty as specialty').where('id').equal(self.id).flatten().getItem().then(student => {
            if (_.isNil(student)) {
                return new HttpNotFoundError();
            }
            // get student courses
            return self.context.model('StudentCourse').where('student').equal(self.id)
                .select('course').flatten().getAllItems().then(studentCourses => {
                    const studentCoursesIds = studentCourses.map(studentCourse => studentCourse.course);
                    // return specialization courses that do not exist in student courses
                    return self.context.model('SpecializationCourse')
                        .where('specializationIndex').equal(-1)
                        .or('specializationIndex').equal(student.specialty)
                        .and('studyProgramCourse/studyProgram').equal(student.studyProgram)
                        .and('studyProgramCourse/course/isEnabled').equal(1)
                        .and('studyProgramCourse/course/courseStructureType').notEqual(8)
                        .and('studyProgramCourse/course').notIn(studentCoursesIds)
                        .prepare();
                }); 
        });
    }


    availableProgramSpecialties(callback) {
        const self = this, context = self.context;
        self.programOf(function(err,program) {
            if (err) {
                TraceUtils.error(err);
                return callback(err);
            }
            if (_.isNil(program)) {
                return callback(new Error('Student program is null or undefined'));
            }
            context.model("StudyProgramSpecialty").where("studyProgram").equal(program).and("specialty").notEqual(-1).getAllItems().then(value => {
                return callback(null, value);
            }).catch( err => {
                return callback(err);
            });
        });
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
     @EdmMapping.func('availableClasses',EdmType.CollectionOf('StudentAvailableClass'))
     getAvailableClasses() {
         const self = this;
         return self.getModel().select('id','department').where('id').equal(self.id).expand('department').getItem().then(function(result) {
             if (_.isNil(result)) {
                 return Promise.reject(new HttpNotFoundError());
             }
             const entityExpr = util.format('ufnStudentAvailableClasses(%s,%s,%s)',result.department.currentYear, result.department.currentPeriod, result.id);
             const entity = new QueryEntity(entityExpr).as('StudentAvailableClasses');
             const q = self.context.model('StudentAvailableClass').asQueryable();
             //replace entity reference
             q.query.from(entity);
             //return queryable
             return Promise.resolve(q);
         }).catch(function(err) {
             TraceUtils.error(err);
             if (err instanceof HttpError) {
                 return Promise.reject(err);
             }
             return Promise.reject(new HttpServerError());
         });
     }

    /**
     * @swagger
     *
     * /api/Students/Me/CanSelectSpecialty:
     *  get:
     *    tags:
     *      - Student
     *    description: Validates if student can select a study program specialty
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func('canSelectSpecialty', ValidationResult)
    async canSelectSpecialty() {
        // check if student is active
        const active = await this.isActive();
        // if student is not active
        if (!active) {
            // throw error
            return new ValidationResult(false, 'ESTATUS', 'Student is not active');
        }
        // get student study program
        const studyProgram = await this.property('studyProgram').expand('specialties').getItem();
        // validate study program
        if (studyProgram == null) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program is null or undefined.');
        }
        if (!Array.isArray(studyProgram.specialties)) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program specialties cannot be validated.');
        }
        if (studyProgram.specialties.length === 0) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program does not have any specialty.');
        }
        // get student current specialty
        const currentSpecialty = await this.getSpecialty();
        if (currentSpecialty && studyProgram.specialties.length === 1) {
            return new ValidationResult(false, 'ESPECIALTY', 'Study program does not have an available specialty other than student current specialty.');
        }

        // check department settings only if context.user is student
        const isMe = await this.is(':me');

        // check if current specialty is other than base specialty
        if (isMe && currentSpecialty && currentSpecialty.specialty > -1) {
            return new ValidationResult(false, 'ESPECIALTY', 'Student has already a study program specialty.');
        }
        // get student department
        const studentDepartment = await this.property('department').select('id').value();
        let department = {};
        if (isMe) {
            // get local department and validate registration period
            // get current date
            const fromDate = new Date(); // get current date with time
            const toDate = moment(new Date()).startOf('day').toDate(); // get only date format
            department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
                .and('date(registrationPeriodStart)').lowerOrEqual(fromDate)
                .and('date(registrationPeriodEnd)').greaterOrEqual(toDate)
                .select('id', 'name', 'registrationPeriodStart', 'registrationPeriodEnd', 'allowSpecialtySelection', 'currentYear', 'currentPeriod').flatten().getItem();
            if (department == null) {
                return new ValidationResult(false, 'EPERIOD', 'Invalid student registration period.');
            }
            // validate allow specialty selection
            if (!department.allowSpecialtySelection) {
                return new ValidationResult(false, 'ESPECIALTY', 'Student department does not allow changing specialty.');
            }
        } else {
            department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
                .select('id', 'name', 'registrationPeriodStart', 'registrationPeriodEnd', 'allowSpecialtySelection', 'currentYear', 'currentPeriod').flatten().getItem();
        }
        // calculate student semester for current year and period
        const studentSemester = await new Promise((resolve, reject) => {
            return this.inferSemester(department.currentYear, department.currentPeriod, function (err, value) {
                if (err) {
                    return reject(err);
                }
                try {
                    // validate semester type
                    Args.notNumber(value, 'Student registration semester');
                    // validate semester value
                    Args.check(value > 0, 'Student registration semester must be greater than zero');
                    // return calculated semester
                    return resolve(value);
                } catch (err) {
                    return reject(err);
                }
            });
        });
        if (studyProgram.specialtySelectionSemester && studyProgram.specialtySelectionSemester > studentSemester) {
            return new ValidationResult(false, 'ESEMESTER', 'Invalid student semester for selecting or updating student specialty.');
        }
        return new ValidationResult(true, 'SUCC');
    }


    @EdmMapping.func("availableProgramGroups", EdmType.CollectionOf("ProgramGroup"))
    async availableProgramGroups() {
        // check if student is active
        const self = this, context = self.context;
        const active = await this.isActive();
        // if student is not active
        if (active) {
            // check if context user is the student
            const isMe = await this.is(':me');
            // get student study program
            const studyProgram = await this.property('studyProgram').select('id').value();
            // get student department
            const studentDepartment = await this.property('department').select('id').value();
            let department = null;
            if (!isMe) {
                department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
                    .select('id', 'name', 'currentYear', 'currentPeriod').flatten().getItem();
            }
            else {
                // get local department and validate registration period
                // get current date
                const fromDate = new Date(); // get current date with time
                const toDate = moment(new Date()).startOf('day').toDate(); // get only date format
                department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
                    .and('date(registrationPeriodStart)').lowerOrEqual(fromDate)
                    .and('date(registrationPeriodEnd)').greaterOrEqual(toDate)
                    .select('id', 'name', 'registrationPeriodStart', 'registrationPeriodEnd', 'currentYear', 'currentPeriod').flatten().getItem();
            }
            if (department == null) {
                return [];
            }
            const departmentConfiguration = await context.model('DepartmentConfiguration').where('department').equal(studentDepartment).flatten().silent().getItem();
            // check if department allowProgramGroupSelection is true
           if (isMe) {
               if (departmentConfiguration == null || (departmentConfiguration && !departmentConfiguration.allowProgramGroupSelection)) {
                   return [];
               }
           }
            // get study program groups
            const programGroups = await context.model('ProgramGroup').where('program').equal(studyProgram).and('isActive').equal(1).expand('groupType').getItems();
            if (programGroups.length > 0) {
                // calculate student semester for current year and period
                const studentSemester= await new Promise((resolve, reject) => {
                    return this.inferSemester(department.currentYear, department.currentPeriod, function (err, value) {
                        if (err) {
                            return reject(err);
                        }
                        try {
                            // validate semester type
                            Args.notNumber(value, 'Student registration semester');
                            // validate semester value
                            Args.check(value > 0, 'Student registration semester must be greater than zero');
                            // return calculated semester
                            return resolve(value);
                        } catch (err) {
                            return reject(err);
                        }
                    });
                });

                // get super groups
                const superGroups = programGroups.filter(x => {
                    return x.groupType.alternateName === 'super';
                });
                if (superGroups.length > 0) {
                    // get student current program groups
                    const studentGroups = await context.model('StudentProgramGroup').where('student').equal(self.getId()).expand('programGroup').silent().getItems();

                    if (superGroups.length > 0) {
                        superGroups.forEach(superGroup => {
                            // get min semester
                           // const minSemester =
                            let minSemester = 1;
                            if (superGroup.availableInExams !=null && superGroup.availableInExams!==0) {
                                let powers = this.getPowersOfTwo(superGroup.availableInExams);
                                minSemester = powers.reduce((prev, curr) => {
                                    return Math.min(prev, curr)
                                }) + 1;
                            }
                            if (superGroup.minCourses > 0 && studentSemester>=minSemester) {
                                const numberOfStudentGroups = studentGroups.filter(x => {
                                    return x.programGroup.parentGroup === superGroup.id;
                                }).length;
                                if (numberOfStudentGroups < superGroup.minCourses) {
                                    // add to available groups, remove those that exist at student groups
                                    superGroup.groups = programGroups.filter(x => {
                                        return x.parentGroup === superGroup.id;
                                    });
                                    superGroup.groups.forEach(group => {
                                        const studentGroup = studentGroups.find(x => {
                                            return x.programGroup.id === group.id;
                                        });
                                        if (studentGroup) {
                                            //remove from groups, student has already this group
                                            superGroup.groups = superGroup.groups.filter(y => {
                                                return y.id !== studentGroup.programGroup.id;
                                            });
                                        }
                                    });
                                    superGroup.maxAllowedGroups = superGroup.minCourses - numberOfStudentGroups;
                                } else {
                                    //remove superGroup from available groups
                                    superGroup.maxAllowedGroups = 0;
                                }
                            }
                            else {
                                superGroup.maxAllowedGroups = 0;
                            }
                        });
                    }
                    return superGroups.filter(x => {
                        return x.maxAllowedGroups > 0;
                    });
                }
            }
        }
        return [];
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func('programGroupActions', EdmType.CollectionOf('UpdateStudentGroupAction'))
    async getProgramGroupActions() {
        return await this.context.model('UpdateStudentGroupAction').where('object').equal(this.getId()).prepare();
    }
    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func('programGroups', EdmType.CollectionOf('UpdateStudentGroupAction'))
    async getStudentProgramGroups() {
        return await this.context.model('StudentProgramGroup').where('student').equal(this.getId()).prepare();
    }



    /**
     * @returns {UpdateStudentGroupAction}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('programGroupActions', 'UpdateStudentGroupAction')
    async saveProgramGroupAction(data) {
        // save action
        const action = await this.context.model('UpdateStudentGroupAction').save(data);
        // return original updateStudentGroup action
        return await this.context.model('UpdateStudentGroupAction').where('id').equal(action.id).getItem();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/RegisterActions:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns a collection of student register actions
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func('RegisterActions', EdmType.CollectionOf('StudentRegisterAction'))
    /**
     * @return DataQueryable
     */
    getRegisterActions() {
        return this.context.model('StudentRegisterAction').where('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns an object which represents a register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */

    /**
     * @returns {Promise<StudentRegisterAction>}
     */
    @EdmMapping.func('CurrentRegisterAction', 'StudentRegisterAction')
    async getCurrentRegisterAction() {
        // noinspection JSCheckFunctionSignatures
        let q = await this.context.model('StudentRegisterAction')
            .filter('registrationYear eq $it/object/department/currentYear and registrationPeriod eq $it/object/department/currentPeriod');
        return q.and('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  post:
     *    tags:
     *      - Student
     *    description: Save student register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * @returns {Promise<StudentRegisterAction|*>}
     */
    @EdmMapping.action('CurrentRegisterAction', 'StudentRegisterAction')
    async saveCurrentRegisterAction() {
        // get silent mode from parent
        const silent = this.getModel().isSilent();
        // get current academic year and period
        const student = await this.context.model('Student')
            .where('id').equal(this.getId())
            .select('id', 'department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .silent()
            .getItem();
        // set object equal to this
        const registerAction ={
            object: student.id,
            registrationYear: student.currentYear,
            registrationPeriod: student.currentPeriod
        };
        // save register action
        await this.context.model('StudentRegisterAction').silent(silent).save(registerAction);
        // return original register action
        return await this.context.model('StudentRegisterAction').where('id').equal(registerAction.id).silent(silent).getItem();
    }

    async isActive() {
        return await this.getModel().where('id').equal(this.getId())
            .and('studentStatus/alternateName').equal('active').silent().count();
    }
    /**
     * @swagger
     *
     * /api/Students/Me/CalculateGraduationDegree:
     *  post:
     *    tags:
     *      - Student
     *    description: Calculate student graduation degree
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/CalculationRule'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * @returns {Promise<Object|*>}
     */
    @EdmMapping.action('CalculateGraduationDegree',  'Object')
    async calculateGraduationDegree() {
        /**
         * @type Student
         */
        const context = this.context;
        const student = await context.model('Student').where("id").equal(this.getId()).silent().getTypedItem();
        const studyProgram = await context.model('StudyProgram').where('id').equal(student.studyProgram).silent().getItem();
        //get programGroupDecimalDigits from student department
        const programGroupDecimalDigits = await context.model('LocalDepartment').where('id').equal(student.department).select('programGroupDecimalDigits').silent().value();

        const gradeScale = await context.model('GradeScale').where('id').equal(studyProgram.gradeScale).expand('values').silent().getTypedItem();
        let intermediateDecimalDigits = 3;
        // get intermediateDecimalDigits from settings
        let universisConfiguration = context.getApplication().getConfiguration().getSourceAt('settings/universis');
        if (typeof universisConfiguration !== 'undefined') {
            intermediateDecimalDigits = universisConfiguration.intermediateDecimalDigits || 3;
        }
        // get student's study program calculationRules
        const calculationRules = await context.model('CalculationRules').where('studyProgram').silent().equal(student.studyProgram)
            .expand('ruleType').getItems();

        //get student program groups with group grade
        const studentGroups = await context.model('StudentProgramGroup').where('student').equal(student.id)
            .and('calculateGroupGrade').equal(1)
            .expand({
                'name': 'programGroup',
                'options': {
                    '$expand': 'gradeScale'
                }
            }).levels(3).silent()
            .getItems();

        // get passed student courses calculated at degree
        const studentCourses = await context.model('StudentCourse').where('student').equal(student.id)
            .and('isPassed').equal(1)
            .and('courseStructureType').in([1, 4])
            .and('calculateGrade').equal(1)
            .expand('courseType', 'semester', 'programGroup', 'examPeriod', {
                'name': 'course',
                'options': {
                    '$expand': 'gradeScale'
                }
            })
            .silent()
            .getItems();

        // get student thesis
        const studentTheses = await context.model('StudentThesis').where('student').equal(student.id)
            .and('thesis/status/alternateName').equal('completed')
            .expand({
                'name': 'thesis',
                'options': {
                    '$expand': 'gradeScale'
                }
            }).silent()
            .getItems();

        if (calculationRules && calculationRules.length === 0) {
            return [];
        } else {
            // calculating rules
            calculationRules.forEach(calculationRule => {
                switch (calculationRule.ruleType.alternateName) {
                    case "CourseType":
                    case "Semester":
                    case "CourseCategory":
                        calculationRule.courses = studentCourses;
                        if (studentGroups.length > 0) {

                            // add student groups to courses
                            studentGroups.forEach(studentGroup => {
                                // remove courses that belong to student groups and are calculated as a single course
                                calculationRule.courses = calculationRule.courses.filter(x => {
                                    return (!x.programGroup || (x.programGroup && x.programGroup.id != studentGroup.programGroup.id));
                                });
                                if (studentGroup.calculateGrade === 1) {
                                    if (studentGroup.isPassed === 1) {
                                        // add to courses
                                        calculationRule.courses.push({
                                            type: 'ProgramGroup',
                                            grade: studentGroup.grade,
                                            coefficient: studentGroup.programGroup.gradeScale.scaleType !== 3 ? studentGroup.coefficient : 0,
                                            course: studentGroup.programGroup,
                                            gradeScale: studentGroup.programGroup.gradeScale,
                                            product: studentGroup.programGroup.gradeScale.scaleType !== 3 ? studentGroup.grade * studentGroup.coefficient : 0
                                        });
                                    } else {
                                        // sometimes group grade has not been calculated
                                        //check if group courses are passed
                                        const groupCourses = studentCourses.filter(x => {
                                            return x.programGroup && x.programGroup.id === studentGroup.programGroup.id && x.isPassed === 1
                                        });
                                        if (groupCourses.length >= studentGroup.maxCourses) {
                                            // calculate student group grade
                                            const percent = groupCourses.reduce((partial_sum, a) => partial_sum + a.groupPercent, 0);
                                            studentGroup.grade = percent > 0 ? round(groupCourses.reduce((partial_sum, a) => partial_sum + a.grade * a.groupPercent, 0) / percent, programGroupDecimalDigits + 1) : 0;
                                            const studentGroupGradeScale = context.model('GradeScale').convert(studentGroup.programGroup.gradeScale);
                                            if (studentGroupGradeScale.scaleType !== 0) {
                                                //use gradeScale to get the exact value for gradeScales with values e.g. 0-10 step 0.5
                                                studentGroup.grade = studentGroupGradeScale.convertTo(studentGroup.grade);
                                                studentGroup.grade = studentGroupGradeScale.convertFrom(studentGroup.grade);
                                            }
                                            calculationRule.courses.push({
                                                type: 'ProgramGroup',
                                                grade: studentGroup.grade,
                                                coefficient: studentGroup.programGroup.gradeScale.scaleType !== 3 ? studentGroup.coefficient : 0,
                                                course: studentGroup.programGroup,
                                                gradeScale: studentGroup.programGroup.gradeScale,
                                                product: studentGroup.programGroup.gradeScale.scaleType !== 3 ? studentGroup.grade * studentGroup.coefficient : 0
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        if (calculationRule.checkValues != -1) {
                            // rule has been set for specific courseTypes
                            if (calculationRule.ruleType.alternateName === 'CourseType') {
                                //filter by courseTypes
                                calculationRule.courses = calculationRule.courses.filter(x => {
                                    return calculationRule.checkValues.split(',').findIndex(y => {
                                        return x.courseType.id == y;
                                    }) >= 0;
                                });
                            } else {
                                if (calculationRule.ruleType.alternateName === 'Semester') {
                                    //filter by semester
                                    calculationRule.courses = calculationRule.courses.filter(x => {
                                        return calculationRule.checkValues.split(',').findIndex(y => {
                                            return x.semester.id == y;
                                        }) >= 0;
                                    });
                                } else {
                                    // filter by course category
                                    calculationRule.courses = calculationRule.courses.filter(x => {
                                        return calculationRule.checkValues.split(',').findIndex(y => {
                                            return x.course.courseCategory.id == y;
                                        }) >= 0;
                                    });
                                }
                            }

                        }

                        calculationRule.courses = calculationRule.courses.map(studentCourse => {
                            return studentCourse = {
                                type: 'Course',
                                course: studentCourse.course,
                                grade: studentCourse.grade,
                                coefficient: studentCourse.course.gradeScale.scaleType !== 3 ? studentCourse.coefficient : 0,
                                programGroup: studentCourse.programGroup,
                                gradeScale: studentCourse.course.gradeScale,
                                product: studentCourse.course.gradeScale.scaleType !== 3 ? studentCourse.grade * studentCourse.coefficient : 0
                            }
                        });
                        break;
                    case 'StudentThesis':
                        // add thesis to calculationRule
                        calculationRule.courses = studentTheses.map(thesis => {
                            return thesis = {
                                type: 'Thesis',
                                course: thesis.thesis,
                                grade: thesis.grade,
                                coefficient: thesis.thesis.gradeScale.scaleType !== 3 ? thesis.thesis.coefficient : 0,
                                gradeScale: thesis.thesis.gradeScale,
                                product: thesis.thesis.gradeScale.scaleType !== 3 ? thesis.grade * thesis.thesis.coefficient : 0
                            }
                        });
                        break;
                }
            });
            //build
            let graduationGrade = 0;
            let sumOfCoefficients = 0;
            let totalProduct = 0;
            let totalCoefficients = 0;
            let unit = false;
            calculationRules.forEach(calculationRule => {
                if (calculationRule.courses) {
                    calculationRule.sumOfProduct = round(calculationRule.courses.reduce((partial_sum, a) => partial_sum + a.product, 0), 5);
                    calculationRule.sumOfCoefficient = calculationRule.courses.reduce((partial_sum, a) => partial_sum + a.coefficient, 0);
                    calculationRule.final = calculationRule.sumOfCoefficient > 0 ? (round(calculationRule.sumOfProduct / calculationRule.sumOfCoefficient, intermediateDecimalDigits)) * calculationRule.coefficient : 0;
                    sumOfCoefficients += calculationRule.coefficient * 1;
                    graduationGrade += round(calculationRule.final || 0, intermediateDecimalDigits);
                    if (calculationRule.coefficient == 1) {
                        unit = true;
                        totalProduct += calculationRule.sumOfProduct;
                        totalCoefficients += calculationRule.sumOfCoefficient;
                    }

                }
            });
            if (unit === true) {
                graduationGrade = totalCoefficients > 0 ? round(round(totalProduct / totalCoefficients, 6), gradeScale.formatPrecision + 1) : 0;
            }

            let response = {
                "calculationRules": calculationRules,
                "graduationGrade": round(graduationGrade, studyProgram.decimalDigits + 1),
                "gradeScale": gradeScale.id,
                "studentGraduationGrade": student.graduationGrade,
                "degreeDescription": studyProgram.degreeDescription
            };
            response.formattedGrade = gradeScale.convertTo(response.graduationGrade);
            // get graduation degree in words
            const formatter = new NumberFormatter();
            const locale = context.locale;
            const decimalSeparator = new Intl.NumberFormat(locale).format(1.1).substring(1,2);
            try {
                response.graduationGradeWrittenInWords = {
                    upperCase: gradeScale.scaleType === 0 ? formatter.format(parseFloat(response.formattedGrade.replace(decimalSeparator, '.')), locale, studyProgram.decimalDigits).toLocaleUpperCase(locale) : response.formattedGrade,
                    lowerCase: gradeScale.scaleType === 0 ? formatter.format(parseFloat(response.formattedGrade.replace(decimalSeparator, '.')), locale, studyProgram.decimalDigits).toLocaleLowerCase(locale) : response.formattedGrade
                };
            }
            catch (e) {
                TraceUtils.error(`Error calculating graduationGradeWrittenInWords for student with id ${student.id}`);
            }
            // calculate final date
            response.lastObligation = await this.calculateLastDate();
            return response;
        }
    }

    /**
     * @swagger
     * /api/Students/Me/Specialty:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns student's current study program specialty
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/StudyProgramSpecialty'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * Gets study program specialty
     * @returns {Promise<StudyProgramSpecialty>}
     */
    @EdmMapping.func('specialty', 'StudyProgramSpecialty')
    async getSpecialty() {
        // get student study program and specialty
        return this.property('studyProgramSpecialty').getItem();
    }

    /**
     * @swagger
     *
     *  /api/Students/Me/Specialty:
     *   post:
     *    tags:
     *      - Student
     *    description: Initializes an action for selecting a study program specialty by student. This action may be automatically completed.
     *    security:
     *     - OAuth2:
     *        - students
     *    requestBody:
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/components/schemas/StudyProgramSpecialty'
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/UpdateSpecialtyAction'
     *      '404':
     *        description: not found
     *      '409':
     *        description: conflict
     *      '500':
     *        description: internal server error
     */
    /**
     * Gets study program specialty
     * @returns {Promise<StudyProgramSpecialty>}
     */
    @EdmMapping.param('specialty', 'StudyProgramSpecialty', false, true)
    @EdmMapping.action('specialty', 'UpdateSpecialtyAction')
    async updateSpecialty(specialty) {
            if (specialty == null) {
                throw new HttpBadRequestError('Expected a study program specialty');
            }
            // create a new update specialty action
            const action = {
                object: this,
                specialty: specialty
            };
            // select specialty validation
            const validationResult = await this.canSelectSpecialty();
            // if validation has been failed
            if (validationResult.success === false) {
                // send conflict error
                throw Object.assign(new HttpConflictError(validationResult.message), {
                    code: validationResult.code
                })
            }
            // check if an active action already exists
            let exists = await this.context.model('UpdateSpecialtyAction')
                .where('object').equal(this.id).and('actionStatus/alternateName').equal(ActionStatusType.ActiveActionStatus)
                .count();
            // and throw conflict error
            if (exists) {
                throw Object.assign(new HttpConflictError('An active update specialty action already exists.'), {
                    code: 'ECONSTRAINT'
                });
            }
            // save update specialty action
            await this.context.model('UpdateSpecialtyAction').save(action);
            // return result
            const result = await this.context.model('UpdateSpecialtyAction').where('id').equal(action.id).getItem();
            result.validationResult=action.validationResult;
            return result;
    }

    /**
     * @swagger
     *
     * /api/Students/Me/theses:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns an object which represents student theses for current student
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func('theses', EdmType.CollectionOf('StudentThesis'))
    getStudentTheses() {
        return this.context.model('StudentThesis').where('student').equal(this.getId()).prepare();
    }

    /**
     * Returns a string which represents a description of an item e.g. "[145100-10] Stuart Peers" etc
     */
    async getDescription() {
        const item = await this.getModel().where('id').equal(this.id).select(
            'studentIdentifier', 
            'person/familyName as familyName',
            'person/givenName as givenName',
            ).getItem();
        if (item) {
            return `[${item.studentIdentifier}] ${item.familyName} ${item.givenName}`
        }
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func('StudentRemoveActions', EdmType.CollectionOf('StudentRemoveAction'))
    getStudentRemoveActions() {
        return this.context.model('StudentRemoveAction').where('student').equal(this.getId()).prepare();
    }

    /**
     * @returns {Action}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('StudentRemoveActions', 'StudentRemoveAction')
    async saveStudentRemoveActions(data) {
        // save action
        const action = await this.context.model('StudentRemoveAction').save(data);
        // return original updateStudentGroup action
        return await this.context.model('StudentRemoveAction').where('id').equal(action.id).getItem();
    }

    /**
     * @returns {Student}
     */
    @EdmMapping.func("currentSemester","Object" )
    async getCurrentSemester() {

        /**
         * @type {Student}
         */
        const student = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('id', 'semester', 'department/currentYear as currentYear', 'department/currentPeriod as currentPeriod', 'inscriptionYear', 'inscriptionPeriod', 'inscriptionSemester', 'studentStatus/alternateName as studentStatus')
            .flatten()
            .getItem();
        let semester = student.semester;
        if (student.studentStatus === 'active') {
            // get last suspension if any
            const suspension = await this.context.model('StudentSuspension')
                .where('student').equal(this.id)
                .flatten()
                .orderByDescending('identifier').getItem();
            if (suspension) {
                if (suspension.reintegrated) {
                    semester = (student.currentYear - suspension.reintegrationYear) * 2 + (student.currentPeriod - suspension.reintegrationPeriod) + suspension.reintegrationSemester;
                }
            } else {
                semester = (student.currentYear - student.inscriptionYear) * 2 + (student.currentPeriod - student.inscriptionPeriod) + student.inscriptionSemester;
            }
        }
        if (semester < 1) {
            semester = student.semester;
        }
        return {value: semester};
    }

    /**
     * Returns a collection of rules which are going to be check while validating that student is eligible for graduation
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('StudentGraduationRules', EdmType.CollectionOf('Rule'))
    async getStudentGraduationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'Student', 'GraduateRule');
    }

    /**
     * Updates student graduation rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('StudentGraduationRules', EdmType.CollectionOf('Rule'))
    async setStudentGraduationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'Student';
            // set additional type
            converted.additionalType = 'GraduateRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getStudentGraduationRules();
    }
    @EdmMapping.func("reports",EdmType.CollectionOf("ReportTemplate"))
    getAvailableReports() {
        return this.context.model('ReportTemplate').where('reportCategory/appliesTo').in(['Student']).prepare();
    }

    @EdmMapping.func("internships",EdmType.CollectionOf("Internship"))
    getStudentInternships() {
        return this.context.model('Internship').where('student').equal(this.getId()).prepare();
    }

    @EdmMapping.func("periodRegistrationTrace", EdmType.CollectionOf('StudentRegistrationTrace'))
    static getStudentAnalytics(context) {
        // returns all student period registrations referred to retroactive year and period
        return context.model('StudentRegistrationTrace').asQueryable().prepare();
    }

    @EdmMapping.func("counselors",EdmType.CollectionOf("StudentCounselor"))
    getStudentCounselors() {
        return this.context.model('StudentCounselor').where('student').equal(this.getId()).prepare();
    }

    async calculateLastDate () {
        const self = this, context = self.context;
        const result = {
            resultDate: null,
            type: null,
            object: null
        };
        const lastStudentCourse = await context.model('StudentCourses').where('student').equal(self.getId())
            .and('isPassed').equal(1).and('calculateGrade').equal(1)
            .and('gradeExam/resultDate').notEqual(null)
            .and('course/courseStructureType').notEqual(4)
            .expand('gradeExam')
            .silent()
            .orderByDescending('gradeExam/resultDate').getItem();
        if (lastStudentCourse) {
            result.type = 'StudentCourse';
            result.object = lastStudentCourse;
            result.resultDate = lastStudentCourse.gradeExam && lastStudentCourse.gradeExam.resultDate;
        }
        // get last thesis completed date
        const thesis = await context.model('StudentThesis').where('student').equal(self.getId())
            .and('thesis/status/alternateName').equal('completed')
            .and('dateCompleted').notEqual(null)
            .silent()
            .orderByDescending('dateCompleted').getItem();
        if (thesis) {
            //compare dates
            if (moment(result.resultDate).isBefore(thesis.dateCompleted)) {
                result.type = 'StudentThesis';
                result.object = thesis;
                result.resultDate = thesis.dateCompleted;
            }
        }

        const internship = await context.model('Internship').where('student').equal(self.getId())
            .and('status/alternateName').equal('completed')
            .and('dateCompleted').notEqual(null)
            .silent()
            .orderByDescending('dateCompleted').getItem();
        if (internship) {
            //compare dates
            if (moment(result.resultDate).isBefore(internship.dateCompleted)) {
                result.type = 'Internship';
                result.object = internship;
                result.resultDate = internship.dateCompleted;
            }
        }
        return result;
    }

    @EdmMapping.func('teachingEvents', EdmType.CollectionOf('TeachingEvent'))
    async getTeachingEvents() {
        // let registration = await (await this.getCurrentRegistration()).select('classes/courseClass as courseClass', 'classes/section as section').getItems();
        // let events = []
        // for (const studentClass of registration) {
        //     let items = studentClass.section != null ? await this.context.model('TeachingEvent')
        //             .where('courseClass').equal(studentClass.courseClass)
        //             .expand('sections')
        //             .and('sections/section').equal(studentClass.section)
        //             .getItems() :
        //         await this.context.model('TeachingEvent')
        //             .where('courseClass').equal(studentClass.courseClass)
        //             .getItems()
        //     events.push(...items);
        // }
        // return events;
        return this.context.model('TeachingEvent')
            .where('courseClass/period').equal('$it/courseClass/course/department/currentPeriod')
            .and('courseClass/year').equal('$it/courseClass/course/department/currentYear')
            .and('courseClass/students/student').equal(this.getId())
            .prepare();
    }
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    async addAttachment(file, extraAttributes) {
        // append extra attributes
       if (extraAttributes) {
            _.forEach(this.context.model('Attachment').attributeNames, (attribute)=> {
                if (Object.prototype.hasOwnProperty.call(extraAttributes, attribute)) {
                    Object.defineProperty(file, attribute, {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: extraAttributes[attribute]
                    })
                }
            });
        }
        return EnableAttachmentModel.prototype.addAttachment.call(this, file);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    async removeAttachment(attachment) {
        /**
         * validate attachment
         * @type {DataObjectJunction}
         */
        const attachments = this.property('attachments');
        const itemAttachment = await attachments.where('id').equal(attachment).getItem();
        if (itemAttachment == null) {
            throw new DataConflictError('The specified attachment cannot be found or is inaccessible');
        }
        await EnableAttachmentModel.prototype.removeAttachment.call(this, attachment);
        return itemAttachment;
    }

    @EdmMapping.func('photo', EdmType.EdmStream)
    async getStudentPhoto() {
        // get photo from student attachment
        const attachments = this.property('attachments');
        const attachment = await attachments.where('attachmentType/alternateName').equal('studentPhoto').getItem();
        if (attachment) {
            const service = this.context.application.getService(function PrivateContentService() {
            });
            if (service == null) {
                // render with 400 Bad Request
                TraceUtils.warn('PrivateContentService not set');
                return null;
            }
            // get physical path
            const resolvePhysicalPath = util.promisify(service.resolvePhysicalPath).bind(service);
            const physicalPath = await resolvePhysicalPath(this.context, attachment);
            const stream = fs.createReadStream(physicalPath);
            stream.contentType = attachment.contentType;
            return stream;
        }
        else {
            return null;
        }
    }

    /**
     * @returns {Promise<*>}
     */
    getRecipient() {
        return this.getModel().where('id').equal(this.getId()).select('user').silent().value();
    }


    @EdmMapping.func('ProgramSemesterRules',  'Object')
    async getProgramSemesterRulesStatistics(data) {
        const context = this.context;
        /**
         * @type Student
         */
        const student = await context.model('Student').where("id").equal(this.getId()).silent().getTypedItem();
        // get student period registrations to get semesters
        let studentSemesters = await context.model('StudentRegistrationTrace')
            .where("student").equal(this.getId())
            .select('student', 'referenceYear', 'referencePeriod', 'semester')
            .orderBy('semester')
            .silent().getItems();

        // get study program semester rules
        const semesterRules = await context.model('StudyProgramSemesterRules')
            .where('studyProgram').equal(student.studyProgram)
            .and('additionalType').equal('SemesterRule')
            .expand('courseTypes')
            .silent().getItems();

        // get total passed courses grouped by coursetype, year, period
        const studentCourses = await context.model('StudentCourse').where('student').equal(student.id)
            .and('isPassed').equal(1)
            .and('courseStructureType').in([1, 4])
            .select('count(id) as totalCourses', 'sum(units) as totalUnits',
                'sum(ects) as totalEcts',
                'sum(hours) as totalHours',
                'gradeYear', 'gradePeriod', 'courseType')
            .groupBy('gradeYear', 'gradePeriod', 'courseType')
            .flatten()
            .silent()
            .getItems();

        // calculate totals
        studentSemesters.forEach(studentSemester => {
            // get total passed for specific period
            const studentTotals = studentCourses.filter(x => {
                return studentSemester.referenceYear.id === x.gradeYear
                    && studentSemester.referencePeriod.id === x.gradePeriod;
            });
            studentSemester.totalPassed = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalCourses, 0);
            studentSemester.totalPassedEcts = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalEcts, 0);
            studentSemester.totalPassedUnits = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalUnits, 0);
            studentSemester.totalPassedHours = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalHours, 0);

            // get total courses of semester rules (without course types)
            const semesterTotals = semesterRules.find(x => {
                return x.semester === studentSemester.semester && x.courseTypes && x.courseTypes.length === 0;
            });
            studentSemester.totalCourses = semesterTotals ? semesterTotals.courses : 0;
            studentSemester.totalUnits = semesterTotals ? semesterTotals.units : 0;
            studentSemester.totalEcts = semesterTotals ? semesterTotals.ects : 0;
            studentSemester.totalHours = semesterTotals ? semesterTotals.hours : 0;

            // check if there are rules for courseTypes
            const courseTypeRules = semesterRules.filter(x => {
                return x.semester === studentSemester.semester && x.courseTypes && x.courseTypes.length > 0;
            });
            studentSemester.rules=[];
            courseTypeRules.forEach(courseTypeRule => {
                const courseTypeStatisics = studentCourses.filter(x => {
                    return studentSemester.referenceYear.id === x.gradeYear
                        && studentSemester.referencePeriod.id === x.gradePeriod
                        && courseTypeRule.courseTypes.map(y => y.id).includes(x.courseType)
                });
                const rule = {};
                rule.courseTypes = courseTypeRule.courseTypes;
                rule.totals = {
                    courses: courseTypeRule.courses || 0,
                    units: courseTypeRule.units || 0,
                    ects: courseTypeRule.ects || 0,
                    hours: courseTypeRule.hours || 0,
                    totalPassed:  courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalCourses, 0),
                    totalPassedEcts: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalEcts, 0),
                    totalPassedUnits: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalUnits, 0),
                    totalPassedHours: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalHours, 0),
                };
                studentSemester.rules.push(rule);
            });
        });
        return studentSemesters;
    }
}

module.exports = Student;
