import util from 'util';
import {DataObject, DataObjectState, DataPermissionEventListener, EdmMapping, EdmType} from "@themost/data";
import {Args, DataError, HttpBadRequestError} from "@themost/common";
import {promisify} from "es6-promisify";
import {DataConflictError} from "../errors";
const Instructor = require('./instructor-model');
const Rule = require('./rule-model');

@EdmMapping.entityType('CourseClass')
/**
 * @class
 */
class CourseClass extends DataObject {
    constructor() {
        super();
    }

    courseOf(callback) {
        this.attrOf('course', callback);
    }

    label(callback) {
        const self = this;
        try {

            const fnCallback = function(err, result) {
                if (err) {
                    callback(err);
                }
                else if (result) {
                    callback(null, `[${result.displayCode}] ${result.name}`);
                }
                else {
                    callback();
                }
            };
            if (typeof self.course === 'string') {
                self.context.model('Course').where('id')
                    .equal(self.course)
                    .select('name','displayCode')
                    .silent()
                    .first(fnCallback)
            }
            else if (self.course && self.course.displayCode && self.course.name) {
                callback(null, util.format('[%s] %s', self.course.displayCode, self.course.name));
            }
            else {
                delete self.course;
                self.attr('course', fnCallback);
            }
        }
        catch (e) {
            callback(e);
        }
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("students",EdmType.CollectionOf("StudentCourseClass"))
   async getStudents() {
        return this.context.model('StudentCourseClass').where('courseClass').equal(this.getId()).prepare();
    }

    /**
     * @returns {Promise<any>}
     */
    @EdmMapping.param('analytics', EdmType.EdmBoolean, false)
    @EdmMapping.func("sections", EdmType.CollectionOf("CourseClassSection"))
    async getCourseClassSections(analytics) {
        if (analytics) {
            const courseClass = await this.context.model('CourseClass')
                .asQueryable().expand({
                        'name': 'students',
                        'options': {
                            '$select': 'section,courseClass,count(id) as total',
                            '$group': 'section,courseClass'
                        }
                    },
                    {
                        'name': 'sections',
                        'options':{'$expand':'instructors($expand=instructor($select=id,familyName,givenName))'}
                    })
                .levels(5)
                .where('id').equal(this.getId()).silent().getItem();
            return courseClass.sections.map(section => {
                let findSection = courseClass.students.find(studentsSection => {
                    return section.section === studentsSection.section;
                });
                if (findSection) {
                    return Object.assign(section, {'numberOfStudents': findSection.total})
                }
                return section;
            });
        }
        return await this.context.model('CourseClassSection').asQueryable()
            .expand(
                {
                    'name': 'instructors',
                    'options':{
                        '$expand':'instructor($select=id,familyName,givenName)'
                    }
                }
            )
            .where('courseClass').equal(this.getId()).getItems();

    }

    /**
     * @returns {CourseClass}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('copy', 'CourseClass')
    async copyAction(data) {
        /**
         * get CourseClass
         * @type {CourseClass}
         */
        const courseClass = await this.context.model('CourseClass')
            .asQueryable()
            .expand('instructors', 'sections')
            .where('id').equal(this.getId()).silent().getTypedItem();

        let rules = await courseClass.getClassRegistrationRules();

        // check if courseClass already exists
        const exists = await this.context.model('CourseClass').where('course').equal(courseClass.course)
            .and('year').equal(data.year).and('period').equal(data.period).silent().count();
        if (exists) {
            throw new DataError('Course class already exists');
        }
        // remove attributes
        delete courseClass.id;
        delete courseClass.status;
        delete courseClass.statusModified;
        courseClass.year = data.year;
        courseClass.period = data.period;
        courseClass.numberOfStudents = 0;
        if (courseClass.instructors) {
            courseClass.instructors = courseClass.instructors.map(instructor => {
                delete instructor.id;
                delete instructor.courseClass;
                return instructor;
            });
        }
        if (courseClass.sections) {
            courseClass.sections = courseClass.sections.map(section => {
                delete section.id;
                delete section.courseClass;
                return section;
            });
        }
        courseClass.rules = rules;
        // return new CourseClass
        return  await this.context.model('CourseClass').save(courseClass);
    }


    /**
     * @returns {CourseClass}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('createExam', 'CourseExam')
    async createCourseExam(data) {
        try {
            /**
             * get CourseClass
             * @type {CourseClass}
             */
            const courseClass = await this.context.model('CourseClass')
                .asQueryable()
                .expand('instructors')
                .where('id').equal(this.getId())
                .expand({
                        'name': 'course',
                        'options': {
                            '$expand': 'gradeScale'
                        }
                    }, 'instructors'
                )
                .silent().getItem();

            const examPeriod = await this.context.model('ExamPeriod').where('id').equal(data.examPeriod).silent().getItem();
            if (!examPeriod) {
                throw new DataError('Exam period does not exist');
            }
            // check if courseExam already exists
            /**
             * CourseExam
             * @type {CourseExam}
             */
            let courseExam = await this.context.model('CourseExam').where('course').equal(courseClass.course.id)
                .and('year').equal(data.year).and('examPeriod').equal(data.examPeriod)
                .expand('instructors')
                .silent().getItem();
            if (courseExam) {
                //check if courseExam is related to this courseClass
                const exists = await this.context.model('CourseExamClass').where('courseExam').equal(courseExam.id)
                    .and('courseClass').equal(courseClass.id).silent().getItem();
                if (!exists) {
                    // add courseExamClass
                    if ((courseClass.period & examPeriod.periods) === courseClass.period) {
                        //save courseExamClass
                        await this.context.model('CourseExamClasses').save({
                            'courseClass': courseClass.id,
                            'courseExam': courseExam.id
                        });
                        // save courseClass instructors
                        if (courseClass.instructors) {
                            let instructors = courseExam.instructors;
                            courseClass.instructors.forEach(instructor => {
                                const found = instructors.find(x => {
                                    return x.instructor === instructor.instructor;
                                });
                                if (!found) {
                                    instructors.push({
                                        'instructor': instructor.instructor,
                                        'courseExam': courseExam.id
                                    });
                                }
                            });
                            if (instructors.length) {
                                await this.context.model('CourseExamInstructors').save(instructors);
                            }
                        }
                    }
                }
                return courseExam;
            } else {
                // new course exam
                courseExam = {};
                courseExam.year = courseClass.year;
                courseExam.name = courseClass.title;
                courseExam.examPeriod = data.examPeriod;
                courseExam.course = courseClass.course.id;
                courseExam.decimalDigits = courseClass.course.gradeScale.formatPrecision;
                // return new courseExam
                return await this.context.model('CourseExam').save(courseExam);
            }
        } catch (e) {
            throw e;

        }
    }

    /**
     * Returns a collection of rules which are going to be validated during course class registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('RegistrationRules', EdmType.CollectionOf('Rule'))
    async getClassRegistrationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'CourseClass', 'ClassRegistrationRule');
    }

    /**
     * Updates course class registration rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('RegistrationRules', EdmType.CollectionOf('Rule'))
    async setClassRegistrationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'CourseClass';
            // set additional type
            converted.additionalType = 'ClassRegistrationRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getClassRegistrationRules();
    }

    /**
     * Returns a collection of rules which are going to be validated during class section registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('SectionRegistrationRules', EdmType.CollectionOf('Rule'))
    async getSectionRegistrationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'CourseClass', 'SectionRegistrationRule');
    }

    /**
     * Updates class section registration rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('SectionRegistrationRules', EdmType.CollectionOf('Rule'))
    async setSectionRegistrationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'CourseClass';
            // set additional type
            converted.additionalType = 'SectionRegistrationRule';
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getSectionRegistrationRules();
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("teachingEvents",EdmType.CollectionOf("TeachingEvent"))
    getTeachingEvents() {
        return this.context.model('TeachingEvent').where('courseClass').equal(this.getId()).prepare();
    }

    @EdmMapping.param('events', EdmType.CollectionOf('TeachingEvent'), false, true)
    @EdmMapping.action('setTeachingEvents', EdmType.CollectionOf('TeachingEvent'))
    async setTeachingEvents(events){
        const courseClass = await this.context.model('CourseClass').where('id').equal(this.getId()).getItem();
        Args.check(Array.isArray(events), HttpBadRequestError);
        let finalEvents = [];
        for (let item of events) {
            Args.notNull(item, 'TeachingEvents');
            if (item.about !== item.courseClass){
                return new DataConflictError('Fields courseClass and about do not match', null, 'TeachingEvent');
            }
            let sections = await this.getCourseClassSections();
            if(courseClass?.mustRegisterSection){
                if(Array.isArray(sections) && sections.length && item.sections) {
                    if(Array.isArray(item.sections) && item.sections.length) {
                        if(item.sections.every((value => sections.filter(x=> x.section === value || x.id === value).length > 0))){
                            item.sections = sections.flatMap(x => item.sections.filter(value => x.section === value || x.id === value || x.section === value.section)).flatMap(x => sections.filter(val => val.section ===x || val.section === x.section || val.id === x));
                        }
                    } else {
                        throw new DataConflictError('At least one section defined does not exist in course class', 'Make sure all sections exist', 'TeachingEvent')
                    }
                } else if(Array.isArray(sections) && !sections.length){
                    if(Array.isArray(item.sections) && item.sections.length !== 0){
                        throw new DataConflictError('The defined course class does not have any section, but a section was defined.', null, 'TeachingEvent');
                    }
                }else{
                    throw new DataConflictError('Sections defined do not match any course class section associated with this class.')
                }
            }
            finalEvents.push(item);
        }
        try{
            return await this.context.model('TeachingEvent').save(finalEvents);
        } catch (e) {
            return Promise.reject(e)
        }
    }



}

module.exports = CourseClass;
