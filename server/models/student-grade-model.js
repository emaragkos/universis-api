import util from 'util';
import StudentMapper from './student-mapper';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import _ from 'lodash';
import {EdmMapping} from "@themost/data/odata";
import {DataError} from "@themost/common";

@EdmMapping.entityType('StudentGrade')
/**
 * @class
 * @augments DataObject
 * @augments StudentMapper
 */
class StudentGrade extends DataObject {
    constructor() {
        super();
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(self.examGrade) && self.validationResult && self.validationResult.code === 'UNMOD') {
                return callback(null);
            }
            self.studentOf(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(new Error('Student is null or undefined'));
                }
                /**
                 *
                 * @type {Student|DataObject|Array}
                 */
                const student = context.model('Student').convert(result);
                context.unattended(function(cb) {
                    student.is(':active').then(function (active) {
                        if (!active) {
                            return cb(new ValidationResult(false, 'ESTATUS', context.__('Student is not active.')));
                        }
                        else {
                            self.property('courseExam').select('course','year','examPeriod').silent().first(function(err, result) {
                                if (err) {
                                    return cb(err);
                                }
                                if (_.isNil(result)) {
                                    return cb(new Error('Course exam cannot be found.'));
                                }
                                const course=result.course, examYear=result.year, examPeriod=result.examPeriod;
                                const courses = context.model('Course');
                                courses.convert(course).replacedPassed(student.getId(), function (err, passed) {
                                    if (err) {
                                        return cb(err);
                                    }
                                    //get all passed grades for this student
                                    context.model('StudentGrade').where('student').equal(student.getId()).and('course').equal(course.id || course).and('isPassed').equal(1).expand('courseExam').silent().first(function(err,grade) {
                                        if (err) {
                                            return cb(err);
                                        }
                                        if(_.isNil(grade))
                                            return cb();
                                        else {
                                            // if exam grade passed is greater than grade scale base then error
                                            // if exam grade passed is less than grade scale base then Check year and courseExam period
                                            if ((!self.isPassed && examYear > grade.courseExam.year || (examYear === grade.courseExam.year && examPeriod > grade.courseExam.examPeriod))
                                                || (self.isPassed && examYear !== grade.courseExam.year && examPeriod !== grade.courseExam.examPeriod)) {
                                                return cb(new ValidationResult(false, 'EPASS', util.format(context.__("The specified course is already passed [%s-%s %s]"), grade.courseExam.year, grade.courseExam.year + 1, grade.courseExam.examPeriod)));
                                            }
                                            if (passed && self.isPassed) {
                                                // course
                                                return cb(new ValidationResult(false, 'EPASS', context.__("The specified course (or at least one of the courses replaced by this course) has been already passed.")));
                                            }
                                            return cb();
                                        }
                                    });
                                });
                            });

                        }
                    }).catch(function (err) {
                        cb(err);
                    });
                }, function(err) {
                    return callback(err);
                });

            });
        }
        catch (err) {
            callback(err);
        }
    }
    async updateStudentCourse()
    {
        const studentGrade = this, context = this.context;
        // update studentCourse, studentClass with latest grade
        /**
         * @type {StudentCourse}
         */
        let studentCourse = await context.model('StudentCourse')
            .where('student').equal(studentGrade.student)
            .and('course').equal(studentGrade.courseExam.course).getTypedItem();
        if (studentCourse) {
            //check if grade refers to latest examPeriod
            let lastGrade = await studentCourse.getLastGrade();
            if (lastGrade === null) {
                lastGrade = studentGrade;
            }
            if (lastGrade.gradeYear < studentGrade.courseExam.year || (lastGrade.gradeYear === studentGrade.courseExam.year
                && lastGrade.examPeriod <= studentGrade.courseExam.examPeriod)) {
                studentCourse = _.assign(studentCourse, lastGrade);
                await context.model('StudentCourse').save(studentCourse);
            }
        } else {
            throw new DataError(context.__('Course not found'));
        }
        // update studentClass
        const studentClass = await context.model('StudentCourseClass').where('student').equal(studentGrade.student)
            .and('courseClass').equal(studentGrade.courseClass).getItem();
        if (studentClass) {
            const isLastGrade = await context.model('StudentGrade').where('student').equal(studentGrade.student)
                .and('courseClass').equal(studentGrade.courseClass).orderByDescending('courseExam/examPeriod')
                .and('status/alternateName').equal('normal')
                .getItem();
            if (!isLastGrade || (isLastGrade && isLastGrade.id === studentGrade.id)) {
                // update studentClass grade
                studentClass.examGrade = studentGrade.examGrade;
                studentClass.finalGrade = studentGrade.examGrade;
                studentClass.examPeriod =isLastGrade? studentGrade.courseExam.examPeriod: null;
                await context.model('StudentCourseClass').save(studentClass);
            }

        } else {
            throw new DataError(context.__('Student course registration not found'));
        }

    }
}

StudentGrade.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentGrade;
