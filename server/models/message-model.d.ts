import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Account = require('./account-model');
import Action = require('./action-model');
import Attachment = require('./attachment-model');
import User = require('./user-model');

/**
 * @class
 */
declare class Message extends DataObject {

     
     /**
      * @description The subject of this message.
      */
     public subject?: string; 
     
     /**
      * @description The body of this message.
      */
     public body?: string; 
     
     /**
      * @description The email addresses of the author or authors of the message.
      */
     public sender: Account|any; 
     
     /**
      * @description The email addresses of the primary recipients of this message.
      */
     public recipient: Account|any; 
     
     /**
      * @description The email addresses of the secondary recipients of this message.
      */
     public cc?: string; 
     
     /**
      * @description The email addresses of the hidden recipients of this message.
      */
     public bcc?: string; 
     
     /**
      * @description The category of this message.
      */
     public category?: string; 
     
     /**
      * @description The date and time when the message was received.
      */
     public dateReceived?: Date; 
     
     /**
      * @description The user who owns this message.
      */
     public owner: number; 
     
     /**
      * @description The action associated with this message.
      */
     public action?: Action|any; 
     
     /**
      * @description A string that represents a numeric or alphanumeric sequence which identifies uniquely the current item.
      */
     public identifier: string; 
     
     public attachments?: Array<Attachment|any>; 
     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 
     
     /**
      * @description An additional type for this item
      */
     public additionalType?: string; 
     
     /**
      * @description Ένα αναγνωριστικό όνομα για το στοιχείο.
      */
     public alternateName?: string; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     public description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     public image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     public name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     public url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     public createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     public modifiedBy?: User|any; 

}

export = Message;