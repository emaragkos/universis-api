import {DataObject} from '@themost/data/data-object';
import StudyProgram = require('./study-program-model');
import GradeScale = require('./grade-scale-model');
import Semester = require('./semester-model');
import CourseType = require('./course-type-model');

/**
 * @class
 */
declare class ProgramGroup extends DataObject {

     
     /**
      * @description Κωδικός ομάδας προγράμματος σπουδών
      */
     public id: number; 
     
     /**
      * @description Κωδικός προγράμματος σπουδών
      */
     public program: StudyProgram|any; 
     
     /**
      * @description The type of the item.
      */
     public groupType?: string; 
     
     /**
      * @description Κωδικός πατρικής ομάδας
      */
     public parentGroup?: number; 
     
     /**
      * @description Κωδικός εμφάνισης
      */
     public displayCode: string; 
     
     /**
      * @description Ονομασία ομάδας
      */
     public name: string; 
     
     /**
      * @description Περιγραφή
      */
     public description?: string; 
     
     /**
      * @description Ομαδικός βαθμός
      */
     public isCalculated?: string; 
     
     /**
      * @description Κωδικός κλίμακας βαθμολογίας
      */
     public gradeScale?: GradeScale|any; 
     
     /**
      * @description Διδακτικές μονάδες
      */
     public units?: string; 
     
     /**
      * @description Σχόλια
      */
     public comments?: string; 
     
     /**
      * @description Συντελεστής πτυχίου
      */
     public coefficient?: string; 
     
     /**
      * @description Εξάμηνο
      */
     public semester?: Semester|any; 
     
     /**
      * @description Τύπος μαθήματος
      */
     public courseType?: CourseType|any; 
     
     /**
      * @description Ελάχιστος αριθμός μαθημάτων
      */
     public minCourses?: number; 
     
     /**
      * @description Μέγιστος αριθμός μαθημάτων
      */
     public maxCourses?: number; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Program Group Departments
      */
     public dgDepartments?: string; 
     
     /**
      * @description Program Group Areas
      */
     public dgAreas?: string; 
     
     /**
      * @description Ενεργό (ΝΑΙ/ΟΧΙ)
      */
     public isActive?: number; 
     
     /**
      * @description nameEn
      */
     public nameEn?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date;

     getRegistrationRules(): Promise<any>;
     setRegistrationRules(items: any[]): Promise<any>;

}

export = ProgramGroup;
