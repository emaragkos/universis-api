// eslint-disable-next-line no-unused-vars
import {DataEventArgs, DataConfigurationStrategy} from "@themost/data";
/**
 *
 * @param {DataEventArgs} event
 * @returns {Promise<void>}
 */
export async function beforeSaveAsync(event) {
    // get context
    const context = event.model.context;
    // get data configuration
    const dataConfiguration = context.getConfiguration().getStrategy(DataConfigurationStrategy);
    // and model definition
    const definition = dataConfiguration.getModelDefinition(event.model.name);
    // get computed attributes
    const computed = definition.fields.filter((field) => {
       return field.computed === true;
    }).map((field) => {
        return field.name;
    });
    for (let attribute of event.model.attributes) {
        if (computed.indexOf(attribute.name) >= 0) {
            // set attribute model to null in order to exclude it from update or insert
            attribute.model = null;
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback()
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}
