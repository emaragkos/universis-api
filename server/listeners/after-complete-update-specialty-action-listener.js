import ActionStatusType from '../models/action-status-type-model';

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state !== 1) {
        return;
    }
    const target = event.model.convert(event.target);
    const context = event.model.context;
    // build description

    const student = await context.model('Student').where('id').equal(target.object).select('id',
        'studyProgram/name as studyProgram', 'studentIdentifier', 'studyProgramSpecialty/name as studyProgramSpecialty',
        'person/familyName as familyName', 'person/givenName as givenName').getItem();
    const newSpecialty = await context.model('StudyProgramSpecialty').where('id').equal(target.specialty).select('id',
        'studyProgram/name as studyProgram', 'name').getItem();
    if (student) {
        event.target.description = `${context.__('Change study program specialty')}: ${student.studentIdentifier} ${student.familyName} ${student.givenName} (${student.studyProgram}-${student.studyProgramSpecialty}) ->  (${newSpecialty.studyProgram}-${newSpecialty.name})`;
    }
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    const target = event.model.convert(event.target);
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
        // throw error for invalid previous action status
        throw new Error('Invalid action state. An action cannot be completed due to its previous state.');
    }
    // execute action by assigning student and specialty to an instance of StudentSpecialty class
    // get student
    const student = await target.property('object').silent().getItem();
    // get requested specialty
    const specialty = await target.property('specialty').silent().getItem();
    /**
     * get student specialty
     * @type {*}
     */
    const studentSpecialty = event.model.context.model('StudentSpecialty').convert({
        student: student,
        specialty: specialty
    });
    // do save
    await studentSpecialty.save();
    // return validation result
    return studentSpecialty.validationResult;
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state !== 2) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then((validationResult) => {
        event.target.validationResult = validationResult;
        if (validationResult && !validationResult.success)
        {
            return callback(validationResult);
        }
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // execute async method
    return  beforeSaveAsync(event).then((validationResult) => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
