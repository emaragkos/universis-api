import _ from 'lodash';
import async from 'async';
import {ValidationResult} from "../errors";

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function beforeSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {function(Error=)} callback
 */
export function afterSave(event, callback) {
    try {
        const target = event.target, context = event.model.context;
        if (event.state === 1 || event.state === 2) {
            const classes = target['classes'], registrationPeriod = context.model('AcademicPeriod').convert(target.registrationPeriod).getId();
            if (_.isArray(classes)) {
                classes.forEach(function(x) {
                    x.registration = target.id;
                    x.registrationYear = target.registrationYear;
                    x.registrationPeriod = registrationPeriod;
                });
                const studentCourseClasses = context.model('StudentCourseClass');
                //validate student classes
                async.eachSeries(classes, function(item, cb) {
                    try {
                        const studentCourseClass = studentCourseClasses.convert(item);
                        studentCourseClass.validate(function(err, result) {
                            if (err) { return callback(err); }
                            /**
                             * type {ValidationResult[]}
                             */
                            studentCourseClass.validationResults = result || [];
                            const success = (typeof studentCourseClass.validationResults.find(function(x) { return !x.success }) === 'undefined');
                            if (success) {
                                if (studentCourseClass.$state === 2) {
                                    item.validationResult = result[0];
                                    return cb();
                                }
                                //student class registration passed all rules
                                studentCourseClass.save(context,function(err) {
                                   if (err) {
                                       //if err is instance of ValidationResult
                                       if (err instanceof ValidationResult) {
                                           //push validation result to array
                                           item.validationResult=err;
                                       }
                                       else {
                                           //convert error to ValidationResult
                                           item.validationResult=new ValidationResult(false, 'FAIL', 'An internal error occured while registering class.', err.message);
                                       }
                                   }
                                    else {
                                       if (studentCourseClass.validationResults.length === 1) {
                                           item.validationResult = studentCourseClass.validationResults[0];
                                       }
                                       else {
                                           item.validationResult = (new ValidationResult(true, 'SUCC', 'Class registration succeded', null));
                                           item.validationResult.validationResults = studentCourseClass.validationResults || [];
                                       }
                                   }
                                    //exit without error
                                    cb();
                                });
                            }
                            else {
                                if (studentCourseClass.validationResults.length === 1) {
                                    item.validationResult = studentCourseClass.validationResults[0];
                                }
                                else {
                                    item.validationResult = (new ValidationResult(false, 'FAIL', 'Class registration failed', null));
                                    item.validationResult.validationResults = studentCourseClass.validationResults || [];
                                }
                                cb();
                            }
                        });
                    }
                    catch (er) {
                        cb(er);
                    }
                }, function(err) {
                    if (err) { return callback(err); }
                    callback();
                });
            }
            else {
                callback();
            }
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}