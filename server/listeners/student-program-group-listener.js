/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

export function beforeSave(event, callback) {
    return StudentProgramGroupEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function afterExecute(event, callback) {
    return StudentProgramGroupEventListener.afterExecuteAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentProgramGroupEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

class StudentProgramGroupEventListener {

    static async beforeSaveAsync(event) {
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

    }


    /**
     * @param {DataEventArgs} event
     */
    static async afterExecuteAsync(event) {

        // add available attachment types related with graduationEvent (only if one record is returned)
        let data = event['result'];
        // validate data
        if (data == null) {
            return;
        }
        // validate query and exit
        if (event.query.$group) {
            return;
        }
        if (!(Array.isArray(data) && data.length)) {
            return;
        }

        const context = event.model.context;
        for (let i = 0; i < data.length; i++) {
            /**
             * @type {StudentProgramGroup|*}
             */
            let studentGroup = context.model('StudentProgramGroup').convert(data[i]);
            if (studentGroup.calculateGroupGrade === 1 && studentGroup.grade == null) {
                studentGroup = await studentGroup.calculate();
                data[i] = studentGroup;
            }

        }
    }
}
