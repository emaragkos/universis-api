/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import {HttpNotFoundError} from "@themost/common";
import {GenericDataConflictError} from "../errors";
import {DataModel} from "@themost/data";

export function beforeSave(event, callback) {
    return CourseClassSectionEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

export function beforeRemove(event, callback) {
    return CourseClassSectionEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return callback();
}

class CourseClassSectionEventListener {

    static async beforeSaveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state === 1) {
            if (event.target.courseClass) {
                // get courseClassSections
                let lastSection = await context.model('CourseClassSection')
                    .where('courseClass').equal(event.target.courseClass)
                    .select('max(section) as section').value();

                event.target.section = (lastSection || 0) + 1;
                event.target.sectionIndex = event.target.secIndex || event.target.section;

            } else {
                throw new GenericDataConflictError('E_COURSE_CLASS_MISSING', context.__('Course class is missing'), null, 'CourseClassSection');
            }
        }

    }

    static async beforeRemoveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        const section = await context.model('CourseClassSection').where('id').equal(event.target.id).silent().getItem();
        // check student registrations
        const students = await context.model('StudentCourseClass')
            .where('courseClass').equal(section.courseClass)
            .and('section').equal(section.section)
            .select('count(id) as total').silent().value();
        if (students && students > 0) {
            throw new GenericDataConflictError('E_STUDENT_REGISTERED', context.__('Course class section is registered by students'), null, 'CourseClassSection');
        }
        const model=event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings   = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseClassSectionInstructor from mapping before delete
            const mappings = ['CourseClassSectionInstructor'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel)<0;
            });
        };
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

    }
}
