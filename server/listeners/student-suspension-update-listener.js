/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
import {DataError} from "@themost/common";
import Student from '../models/student-model';
import * as _ from 'lodash';

export function beforeSave(event, callback) {
    (async () => {
        // validate student state (on insert or update)
        // todo:: move this action to an upper level
        if (event.state === 1) {
            let context = event.model.context;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(event.target.student);
            const isActive = await student.silent().isActive();
            if (!isActive) {
                throw new DataError('ERR_STATUS',
                    'Invalid student status. Student suspension may execute upon an active student only.'
                    , null,
                    'Student', 'studentStatus');
            }

            // get inscription year and period
           const studentObject = await context.model('Student').where('id').equal(student.id).select('id','inscriptionYear','inscriptionPeriod').silent().flatten().getItem();
            // check suspensionYear year and period
            const suspensionYear = _.isObject(event.target.suspensionYear) ? event.target.suspensionYear.id : event.target.suspensionYear;
            const suspensionPeriod  = _.isObject(event.target.suspensionPeriod) ? event.target.suspensionPeriod.id : event.target.suspensionPeriod;
            if (suspensionYear<studentObject.inscriptionYear || (suspensionYear===studentObject.inscriptionYear  && suspensionPeriod<studentObject.inscriptionPeriod)) {
                throw new DataError('ERR_STATUS',
                    'Invalid suspension year. Student suspension year must be greater or equal than inscription year.'
                    , null,
                    'Student', 'studentStatus');
            }
        }
    })().then( () => {
       return callback();
    }).catch( err => {
        return callback(err);
    });
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    (async () => {
        const context = event.model.context;
        let target;
        if (event.state === 2) {
            // get previous state
            const previousReintegrated = event.previous.reintegrated;
            // get target object
            target = await context.model(event.model.name).where('id').equal(event.target.id).silent().flatten().getTypedItem();
            if (previousReintegrated === 0 && target.reintegrated === 1) {

                const reintegrationYear = target.reintegrationYear;
                const reintegrationPeriod  = target.reintegrationPeriod;
                const suspensionYear =  target.suspensionYear;
                const suspensionPeriod  = target.suspensionPeriod;
                if (_.isNil(reintegrationYear) || _.isNil(reintegrationPeriod) || _.isNil(event.target.reintegrationSemester)) {
                    throw new DataError('ERR_STATUS',
                        'Invalid data. Student reintegration fields should be set'
                        , null,
                        'StudentSuspension', 'reintegrationYear');
                }
                // check reintegration year and period
               if (reintegrationYear<suspensionYear || (reintegrationYear===suspensionYear  && reintegrationPeriod<suspensionPeriod)) {
                    throw new DataError('ERR_STATUS',
                        'Invalid reintegration year. Student reintegration year must be greater or equal than suspension year.'
                        , null,
                        'Student', 'studentStatus');
                }
                // assign id and studentStatus
                const student = context.model('Student').convert(event.target.student);
                student.studentStatus = {
                    alternateName: 'active'
                };
                student.semester = event.target.reintegrationSemester;
                // update student status
                await context.model('Student').silent().save(student);
            }
        } else {
            return;
        }


    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
