import {DataModel} from "@themost/data";
import {DataError, DataNotFoundError} from "@themost/common";

class CourseExamListener {

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const model = event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseExamStudentGrades from mapping before delete
            const mappings = ['CourseExamStudentGrade'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel) < 0;
            });
        };
    }
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        if (event.state === 1 ) {
            // check if course is complex and throw error
            const course = await context.model('Course').where('id').equal(target.course.id || target.course).getItem();
            if (!course) {
                // course does not exist
                throw new DataNotFoundError(context.__('Course not found'));
            }
            if (course.courseStructureType === 4) {
                // creating courseClass for complex course is not allowed
                throw new DataError(context.__('Creating course exam for complex course is not allowed'));
            }
        }
        if (event.state !== 4) {
            if (event.target.year && event.target.examPeriod && event.target.name)
            {
                // get academic year
                const year = context.model('AcademicYear').convert(event.target.year).getId();
                // get exam period
                const examPeriodId = context.model('ExamPeriod').convert(event.target.examPeriod).getId();
                const examPeriod = await context.model('ExamPeriod').where('id').equal(examPeriodId).getItem();
                const academicYear =  await context.model('AcademicYear').where('id').equal(year).getItem();

                // build course exam description
                event.target.description = (`${event.target.name} ${academicYear.alternateName} ${examPeriod.alternateName}`).substr(0,80);
                // set isLate
                if (event.state===1) {
                    event.target.isLate = examPeriod.isLate;
                }
            }

        }
    }
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        if (event.state === 1) {
            // check if courseExam has related classes and instructors
            // get academic year
            const year = context.model('AcademicYear').convert(event.target.year).getId();
            // get course
            const course = context.model('Course').convert(event.target.course).getId();
            // get exam period
            const examPeriodId = context.model('ExamPeriod').convert(event.target.examPeriod).getId();
            const examPeriod =await context.model('ExamPeriod').where('id').equal(examPeriodId).getItem();
            if (!event.target.classes || !Array.isArray(event.target.classes)) {
                // get courseClasses for same course and year
                event.target.classes = [];
                let classes = await context.model('CourseClass')
                    .where('year').equal(year)
                    .and('course').equal(course)
                    .expand('instructors')
                    .silent().getItems();
                if (classes.length > 0) {
                    // filter classes with allowed periods
                    event.target.classes = classes.filter(courseClass => {
                        return (courseClass.period & examPeriod.periods) === courseClass.period;
                    });
                }
            }
            // get instructors from allowed courseClasses
            if (!event.target.instructors || !Array.isArray(event.target.instructors)) {
                event.target.instuctors = [];
                // get instructors
                let instructors = []
                if (event.target.classes) {
                    event.target.classes.forEach(courseClass => {
                        if (courseClass.instructors) {
                            courseClass.instructors.forEach(instructor => {
                                const found = instructors.find(x => {
                                    return x.instructor === instructor.instructor;
                                });
                                if (!found) {
                                    instructors.push({
                                        'instructor': instructor.instructor,
                                        'courseExam': event.target.id
                                    });
                                }
                            });
                        }
                    });
                    event.target.instructors = instructors;
                }
                // save course exam instructors
                if (event.target.instructors && event.target.instructors.length) {
                    await context.model('CourseExamInstructor').save(event.target.instructors);
                }
                // save course exam classes
                if (event.target.classes && event.target.classes.length) {
                    event.target.classes = event.target.classes.map(x => {
                        return {
                            'courseClass': x.id,
                            'courseExam': event.target.id
                        };
                    });
                    await context.model('CourseExamClasses').save(event.target.classes);
                }
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseExamListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseExamListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return CourseExamListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
