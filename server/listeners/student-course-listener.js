import {DataError} from "@themost/common";
import * as _ from 'lodash';
import cloneDeep from "lodash/cloneDeep";
import {GenericDataConflictError} from "../errors";

class StudentCourseListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        if (event.state !== 2) {
            return;
        }
        const target = event.model.convert(event.target);
        const context = event.model.context;
        let previous = event.previous;

        // check if student is active
        const studentId = context.model('Student').convert(target.student).silent().getId();
        const student = await context.model('Student').where('id').equal(studentId).silent().getTypedItem();
        // check if student is active
        const active = await student.silent().isActive();
        // if student is not active
        if (!active) {
            // throw error
            throw new GenericDataConflictError('ESTATUS', context.__('Student is not active.'));
        }
        if (previous.registrationType != target.registrationType) {
            if (previous.registrationType === 1 && target.registrationType === 0) {
                // get last grade of course
                const lastGrade = await target.getLastGrade();
                event.target = _.assign(event.target, lastGrade);
            }
        }
        if ((previous.registrationType === 0 && target.registrationType === 1) || (target.registrationType === 1 && previous.grade !== target.grade)) {

            event.target.examPeriod = null;
            event.target.gradeExam = null;
            if (_.isNil(target.grade)) {
                // grade is not supplied for exemption, set calculateGrade to false
                target.calculateGrade = 0;
            } else {
                if (_.isNil(event.target.course)) {
                    throw new DataError(context.__('Course not found'))
                }
                // check if grade is passed
                const course = await context.model('Course').where('id').equal(event.target.course).select('id', 'gradeScale').expand('gradeScale').silent().getItem();
                if (course) {
                    if (target.grade < course.gradeScale.scaleBase) {
                        throw new DataError(context.__('In case of exemption a passed grade should be provided'))
                    }
                }
            }
        }
        // get programGroup
        if (target.programGroup == null || target.programGroup === '') {
            return;
        }
        // check program group attributes
        const group = await context.model('ProgramGroup').where('id').equal(target.programGroup).and('groupType/alternateName').notEqual('super').silent().getItem();
        if (!group) {
            throw new DataError('ERR_INVALID_DATA',
                context.__('Program group is not available for student course')
                , null,
                'StudentCourse');
        }
        if (group.parentGroup == null || group.parentGroup === 0) {
            return;
        }
        // check studentProgramGroups
        const studentProgramGroup = await context.model('StudentProgramGroup').where('programGroup').equal(group.id).and('student').equal(studentId).silent().getItem();
        if (!studentProgramGroup) {
            throw new DataError('ERR_INVALID_DATA',
                context.__('Program group is not available for student course')
                , null,
                'StudentCourse');
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

        const target = event.target;
        const context = event.model.context;
        if (event.state === 1) {
            // check if course is complex and add also courseParts
            const course = await context.model('Course').where('id').equal(event.target.course).expand('courseParts').silent().getItem();
            if (course.courseStructureType === 4) {
                // add also courseParts
                // get specialization courses
                const courseParts = await context.model('SpecializationCourse')
                    .where('studyProgramCourse/course/parentCourse').equal(course.id)
                    .and('specializationIndex').equal(event.target.specialty)
                    .expand({
                            'name': 'studyProgramCourse',
                            'options': {
                                '$expand': 'course'
                            }
                        }
                    ).silent().getItems();
                const courses = courseParts.map((studentCoursePart => {
                    const coursePart = course.courseParts.find(x => {
                        return x.id === studentCoursePart.studyProgramCourse.course.id;
                    });
                    const studentCourse = cloneDeep(target);
                    delete studentCourse.id;
                    studentCourse.calculateGrade = -studentCourse.calculateGrade;
                    studentCourse.calculateUnits = -studentCourse.calculateUnits;
                    studentCourse.course = coursePart.id;
                    studentCourse.courseTitle = coursePart.name;
                    studentCourse.units = studentCoursePart.units;
                    studentCourse.ects = studentCoursePart.ects;
                    studentCourse.semester = studentCoursePart.semester;
                    studentCourse.coefficient = studentCoursePart.coefficient;
                    studentCourse.courseType = studentCoursePart.courseType;
                    studentCourse.parentCourse = course.id;
                    studentCourse.calculated = coursePart.calculatedCoursePart;
                    studentCourse.coursePercent = coursePart.coursePartPercent;

                    return studentCourse;
                }));
                if (courses.length > 0) {
                    await context.model('StudentCourse').save(courses);
                }
            }
            if (course.courseStructureType === 8) {
                // check if parentCourse exists
                const exists = await context.model('StudentCourse').where('student').equal(target.student)
                    .and('course').equal(course.parentCourse).count();
                if (exists) {
                    return;
                }
                // get specialization parentCourse
                const parentCourse = await context.model('SpecializationCourse')
                    .where('studyProgramCourse/course').equal(course.parentCourse)
                    .and('specializationIndex').equal(event.target.specialty)
                    .expand({
                            'name': 'studyProgramCourse',
                            'options': {
                                '$expand': 'course'
                            }
                        }
                    ).silent()
                    .getItem();

                const studentCourse = cloneDeep(target);
                delete studentCourse.id;
                studentCourse.calculateGrade = -target.calculateGrade;
                studentCourse.calculateUnits = -target.calculateUnits;
                studentCourse.course = course.parentCourse;
                studentCourse.courseTitle = parentCourse.studyProgramCourse.course.name;
                studentCourse.units = parentCourse.units;
                studentCourse.ects = parentCourse.ects;
                studentCourse.semester = parentCourse.semester;
                studentCourse.coefficient = parentCourse.coefficient;
                studentCourse.courseType = parentCourse.courseType;
                studentCourse.parentCourse = null;
                studentCourse.calculated = 0;
                studentCourse.coursePercent = null;

                await context.model('StudentCourse').save(studentCourse);

            }
        }
        if (event.state === 2) {
            const previous = event.previous;
            const programGroup = event.target.programGroup? context.model('ProgramGroup').convert(event.target.programGroup).silent().getId():null;
            let previousGroup = previous.programGroup? context.model('ProgramGroup').convert(previous.programGroup).silent().getId(): null;
            if (previous.grade !== target.grade || previousGroup!==programGroup) {
                // check if course is part of complex course
                if (_.isNil(event.target.course)) {
                    throw new DataError(context.__('Course not found'))
                }
                // check if grade is passed
                const course = await context.model('Course').where('id').equal(event.target.course).silent().getItem();
                if (course && previous.grade !== target.grade) {
                    if (course.courseStructureType===8) {
                        /**
                         *
                         * @type {StudentCourse}
                         */
                        let parentCourse = await context.model('StudentCourse')
                            .where('student').equal(event.target.student)
                            .and('course').equal(course.parentCourse).silent().getTypedItem();

                        const lastGrade = await parentCourse.getLastGrade();
                        parentCourse = _.assign(parentCourse, lastGrade);
                        await context.model('StudentCourse').silent().save(parentCourse);
                    }
                }
                // calculate final program group grade
                // get previous group
                previousGroup =  await context.model('ProgramGroup').where('id').equal(previous.programGroup).silent().getItem();
                const studentCourse = await context.model('StudentCourse').where('id').equal(event.target.id).expand('programGroup').getItem();
                if (studentCourse && ((studentCourse.programGroup && studentCourse.programGroup.parentGroup) || (previousGroup && previousGroup.parentGroup)))
                {
                    const programGroup = (studentCourse.programGroup && studentCourse.programGroup.id) || (previousGroup && previousGroup.id);
                    // calculate program group
                    /**
                     *
                     * @type {StudentProgramGroup}
                     */
                    let studentProgramGroup = await context.model('StudentProgramGroup').where('student').equal(studentCourse.student)
                        .and('programGroup').equal(programGroup).silent().getTypedItem();
                    studentProgramGroup= await studentProgramGroup.calculate();
                    await context.model('StudentProgramGroup').silent().save(studentProgramGroup);

                }
            }
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterRemoveAsync(event) {
        // /**
		//  * @type {DataContext|*}
		//  */
		const context = event.model.context;
        // get student
        const student = event.previous.student.id || event.previous.student;
        // get courseId
        const courseId = event.previous.course.id || event.previous.course;        
        // check if there is a registration for this course
        const registration = await context.model('StudentCourseClass')
            .where('student').equal(student)
            .and('courseClass/course').equal(courseId)
            .silent()
            .count();
        // if a registration exists, the course cannot be removed
        if (registration) {
            throw new DataError(context.__('The course cannot be deleted because it exists in a registration'));
        }
        // get course
        const course = await context.model('Course')
            .where('id').equal(courseId)
            .flatten()
            .select('id', 'courseStructureType')
            .silent()
            .getItem();
        // if the course is complex
        if (course && course.courseStructureType === 4) {
            // get all relevant course parts
			const courseParts = await context.model('Course')
                .where('parentCourse').equal(courseId)
                .select('id')
                .silent()
                .getItems();
            const values = courseParts.map(coursePart => coursePart.id);
            // check if any of them has been registered
            const isRegistered = await context.model('StudentCourseClass')
                .where('student').equal(student)
                .and('courseClass/course').in(values)
                .silent()
                .count();
            if (isRegistered) {
                throw new DataError(context.__('The course cannot be deleted because some of its parts exist in a registration'));
            }
            // get courseParts to be removed
            const removeParts = await context.model('StudentCourse')
                .where('student').equal(student)
                .and('course').in(values)
                .silent()
                .getItems();
            // remove course parts
            await context.model('StudentCourse').remove(removeParts);
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return StudentCourseListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentCourseListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function afterRemove(event, callback) {
    return StudentCourseListener.afterRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
