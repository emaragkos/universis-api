import {DataError} from "@themost/common";
import {ValidationResult} from "../errors";
import {promisify} from "util";

class StudentGradeListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {
        if (event.state === 1) {
            const context = event.model.context;
            const target = event.model.convert(event.target);
            try {
                // find related courseClass
                if (!target.courseClass) {
                    // get latest studentClass
                    const examClasses = await context.model('CourseExamClass')
                        .where('courseExam').equal(target.courseExam)
                        .select('courseClass').getItems();
                    if (examClasses) {
                        const studentCourseClass = await context.model('StudentCourseClass').where('student').equal(target.student)
                            .and('courseClass').in(examClasses.map(x => {
                                return x.courseClass;
                            })).orderByDescending('courseClass/period').getItem();
                        if (!studentCourseClass) {
                            throw new DataError(context.__('Student course registration not found'));
                        }
                        event.target.courseClass = studentCourseClass.courseClass;
                    }
                }
            } catch (err) {
                target.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                throw err;
            }

        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

        const context = event.model.context;
        const previous = event.previous;
        const target = event.model.convert(event.target);

        // get grade status
        const gradeStatus = await target.property('status').getItem();
        // if action status is other that active
        if (gradeStatus.alternateName === 'pending') {
            // exit
            return;
        }
        /**
         * @type {StudentGrade}
         */
        const studentGrade = await context.model('StudentGrade').where('student').equal(target.student)
            .and('courseExam').equal(target.courseExam).expand('courseExam', 'courseClass').getTypedItem();
        if (gradeStatus.alternateName === 'normal') {
            // get previous status
            // get grade status
            const previousStatus = previous ? await context.model('GradeStatus').where('identifier').equal(previous.status).getItem() : null;
            if (event.state === 1 || (event.state === 2 && (previousStatus.alternateName === 'pending' || target.examGrade != previous.examGrade))) {
                // validate grade
                try {
                    const validateAsync = promisify (studentGrade.validate).bind(studentGrade);
                    await validateAsync();
                    // update studentCourse, studentClass with latest grade
                    await studentGrade.updateStudentCourse();
                } catch (err) {
                    target.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                    throw err;
                }
            }
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterRemoveAsync(event) {
        try {
            const context = event.model.context;
            /**
             * @type {StudentGrade}
             */
            const studentGrade =event.model.convert(event.previous);
            studentGrade.courseExam = await context.model('CourseExam').where('id').equal(studentGrade.courseExam).flatten().getItem();
            const res = await studentGrade.updateStudentCourse();
        } catch (err) {
            event.target.validationResult = new ValidationResult(false, 'INVDATA', err.message);
            throw err;
        }
    }

}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return StudentGradeListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentGradeListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return StudentGradeListener.afterRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
