import { DataError } from "@themost/common";
// eslint-disable-next-line no-unused-vars
import { DataObject } from "@themost/data";


class ThesisInstructorUpdateListener {
    /**
     * @param {DataEventArgs} event 
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        if (event.state === 2 && event.previous == null) {
            throw new DataError('E_STATE', 'Previous state cannot be empty while updating.', null, 'Thesis');
        }
        // get thesis object
        const thesis = context.model('Thesis').convert(event.target);
        // query members
        const members = context.model('ThesisRole');
        let previousInstructor;
        if (event.previous && event.previous.instructor) {
                /**
                 * get previous instructor as data object
                 * @type {DataObject}
                 */
                previousInstructor = context.model('Instructor').convert(event.previous.instructor);
        }
        if (Object.prototype.hasOwnProperty.call(event.target, 'instructor')) {
            /**
             * @type {DataObject}
             */
            let instructor;
            // if instructor has been removed and previous instructor is defined
            if ((event.target.instructor == null) && previousInstructor) {
                // try to remove instructor from members
                const member = await members.where('thesis').equal(thesis.id).and('member').equal(previousInstructor.getId()).silent().getItem();
                if (member) {
                    await members.silent().remove(member);
                }

            } else if (previousInstructor) {
                // get instructor object
                instructor = context.model('Instructor').convert(event.target.instructor);
                // check if instructor and previous instructor are different
                if (instructor.id != previousInstructor.id) {
                    // try to remove previous instructor
                    const member = await members.where('thesis').equal(thesis.id).and('member').equal(previousInstructor.getId()).silent().getItem();
                    if (member) {
                        await members.silent().remove(member);
                    }
                }
            } else {
                // get instructor object
                instructor = context.model('Instructor').convert(event.target.instructor);
            }
            // add instructor to members (ThesisRole)
            if ((event.state===1 && instructor!=null) || ( instructor!=null && event.state===2 && previousInstructor && instructor.id != previousInstructor.id)) {
                await members.silent().insert({
                    thesis: thesis,
                    member: instructor,
                    roleName: 'supervisor'
                });
            }

    }
}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return ThesisInstructorUpdateListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

