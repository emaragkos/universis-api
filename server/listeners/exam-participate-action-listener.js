import {GenericDataConflictError} from "../errors";
import moment from "moment";


class ExamParticipateActionListener {
    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        if (event.state === 1) {
            // get student
            const studentId = context.model('Student').convert(event.target.student).getId();
            const student = await context.model('Student').where('id').equal(studentId).getTypedItem();
            // check if student is active
            const active = await student.isActive();
            // if student is not active
            if (!active) {
                // throw error
                throw new GenericDataConflictError('ESTATUS', 'Student is not active');
            }
            // validate participation period info
            const department = await context.model('Student').where('id').equal(student).select('department').silent().value();
            if (department == null) {
                throw new GenericDataConflictError('EDEPT', 'Student department cannot be found.');
            }
            const departmentConfiguration = await context.model('DepartmentConfiguration').where('department').equal(department).flatten().silent().getItem();
            if (departmentConfiguration == null) {
                throw new GenericDataConflictError('EDEPT', 'Student department configuration is not set.');
            }

            // check department configuration
            if (event.target.year != departmentConfiguration.examYear || event.target.examPeriod != departmentConfiguration.examPeriod) {
                throw new GenericDataConflictError('EPERIOD', 'Exam participate action should refer to current department configuration period.');
            }
            // check dates
            const currentDate = new Date();
            const endDate = moment(new Date()).startOf('day').toDate(); // get only date format

            const validDate = departmentConfiguration.examParticipationPeriodStart <= currentDate && departmentConfiguration.examParticipationPeriodEnd >= endDate;
            if (!validDate) {
                throw (new GenericDataConflictError('E_INVALID_PERIOD', context.__('Department exam participation period is closed or it has not been opened yet.')));
            }
            //check student available courseExams
            const availableCourseExams = await student.getAvailableExamsForParticipation();
            const examActions = event.target.courseExamActions || [];
            const courseExamModel = context.model('CourseExam');
            if (Array.isArray(examActions) && examActions.length === 0) {
                throw new GenericDataConflictError('E_MISSING_EXAMS', 'Exams are not included exam participation action.');
            }
            if (Array.isArray(examActions) && examActions.length) {
                // check if exam action belongs to availableCourseExams
                const missingExam = examActions.filter(x => {
                    return x.courseExam != null;
                }).find(x => {
                    return availableCourseExams.findIndex(y => {
                        return courseExamModel.convert(x.courseExam).getId() === y.id;
                    }) < 0;
                });
                if (missingExam) {
                    throw new GenericDataConflictError('E_MISSING_EXAM', 'At least one course exam is not included in available course exams.');
                }
                examActions.forEach(examAction => {
                    examAction.initiator = event.target.id;
                });
                // save courseExamParticipateActions
                await context.model('CourseExamParticipateAction').save(examActions);
                // add a student Message
                //build body from courseExams
                let body='';
                availableCourseExams.forEach(courseExam => {
                    courseExam.agree = examActions.find(x => {
                        return x.courseExam === courseExam.id;
                    }).agree;

                    body += `<div class="pt-1">[${courseExam.course.displayCode}] ${courseExam.name} ${context.__('Participate')}: ${courseExam.agree === 1 ? context.__('Yes:') : context.__('No:')} </div>`;
                    // add test types to body (if any)
                    if (courseExam.types && courseExam.types.length>0 && courseExam.agree === 1) {
                        let testTypes = '';
                        for (let i = 0; i < courseExam.types.length; i++) {
                            testTypes += courseExam.types[i].name + (i < courseExam.types.length -1 ? ', ' : '');
                        }
                        body += `<div class="pt-1">${context.__('TestType')}: ${testTypes}</div>`
                    }
                });
                const message = {
                    student: event.target.student,
                    action: event.target.id,
                    subject: context.__('CompletedExamParticipationSubject'),
                    body: body.substr(0,8000)
                };
                // save student message
                await context.model('StudentMessage').silent().save(message);
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return ExamParticipateActionListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}


