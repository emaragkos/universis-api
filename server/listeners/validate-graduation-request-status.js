import {ApplicationService, TraceUtils} from "@themost/common";
import {DataConfigurationStrategy} from "@themost/data";
import path from "path";
import {GenericDataConflictError} from "../errors";
import ActionStatusType from "../models/action-status-type-model";


/**
 * @param {DataEventArgs} event
 */
export function beforeSave(event, callback) {
    (async function () {
        // check previous and currentStatus
        const context = event.model.context;
        if (event.state === 2) {
            const request = event.model.convert(event.target);
            // get action status
            const actionStatus = request.actionStatus;
            if (actionStatus) {
                // if action status is other that active
                if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                    // exit
                    return;
                }
                // get previous action status
                const previousActionStatus = event.previous && event.previous.actionStatus;
                if (previousActionStatus === null) {
                    throw new Error('Previous status cannot be empty at this context.');
                }
                // validate previous status
                if (previousActionStatus.alternateName === 'ActiveActionStatus' && actionStatus.alternateName === 'CompletedActionStatus') {
                    // only declared students can be approved
                    // check student status
                    const student = await context.model(event.model.name).where('id').equal(event.target.id).select('student').value();
                    const studentStatus =  await context.model('Student').silent().where('id').equal(student)
                        .silent()
                        .select('studentStatus')
                        .value();
                    if (studentStatus) {
                        if (studentStatus.alternateName !== 'declared') {
                            throw new GenericDataConflictError('ERR_STATUS',
                                context.__('Invalid student status. Student graduation request approval may execute upon a declared student only'), null,
                                'Student');
                        }
                    }
                }
            }
        }
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


export class ValidateGraduationRequestStudentStatus extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // call super constructor
        super(app);
        // install service
        this.install();
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get GraduationRequestAction model definition
        const model = configuration.getModelDefinition('GraduationRequestAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: ValidateGraduationRequestStudentStatus service has been successfully installed');
        }
    }

    uninstall() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('GraduationRequestAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.find( listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex>=0) {
            // remove event listener
            model.eventListeners.splice(findIndex, 1);
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: ValidateGraduationRequestStudentStatus service has been successfully uninstalled');
        }
    }
}
