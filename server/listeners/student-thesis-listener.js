import { DataError } from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return StudentThesisListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return StudentThesisListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentThesisListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class StudentThesisListener {

    static async beforeSaveAsync(event) {
        // get context
        if (event.state !== 1) {
            return;
        }
        const context = event.model.context;
        let status;
        const thesis = context.model('Thesis').convert(event.target.thesis).getId();
        // get status
        status = await context.model('Thesis')
            .where('id').equal(thesis)
            .select('status/alternateName')
            .silent()
            .value();
        // throw error of thesis status is either completed or cancelled
        if (status === 'completed' || status === 'cancelled') {
            throw new DataError('E_THESIS_STATUS', context.__('Invalid thesis status. Students of a completed or cancelled thesis cannot be modified.'), null, 'Thesis');
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

        const context = event.model.context;
        if (event.state === 1) {
            // get thesis members
            const thesisRoles = await context.model('ThesisRole').where('thesis').equal(event.target.thesis).getItems();
            if (thesisRoles.length > 0) {
                // add student thesis results
                const results = thesisRoles.map(x => {
                    return {
                        student: event.target.student,
                        instructor: x.member,
                        thesis: x.thesis
                    };
                });
                // save
                await context.model('StudentThesisResult').save(results);
            }
        }
        if (event.state === 2) {
            const previous = event.previous;
            if (previous.grade != event.target.grade) {
                // calculate final thesis grade
                // get all thesis students
                let grade = null;
                const thesisId = context.model('Thesis').convert(event.target.thesis).getId();
                const studentTheses = await context.model('StudentThesis').where('thesis').equal(thesisId).getItems();
                if (studentTheses.length === 1) {
                    grade = studentTheses[0].grade;
                } else if (studentTheses.length > 1) {
                    const thesis = await context.model('Thesis').where('id').equal(thesisId).flatten().getItem();
                    let finalGrade = studentTheses.reduce((partial_sum, a) => partial_sum + a.grade, 0) / studentTheses.length;
                    const gradeScale = await context.model('GradeScale').where('id').equal(thesis.gradeScale).expand('values').getTypedItem();
                    //use gradeScale to get the exact value for gradeScales with values e.g. 0-10 step 0.5
                    finalGrade = gradeScale.convertTo(finalGrade);
                    finalGrade = gradeScale.convertFrom(finalGrade);
                    grade = finalGrade;
                }
                // save thesis grade;
                if (grade) {
                    await context.model('Thesis').save({id: thesisId, grade: grade});
                }
            }
        }
    }

    static async beforeRemoveAsync(event) {
        // get context
        const context = event.model.context;
        let status;
        const studentThesis = await context.model('StudentThesis').where('id').equal(event.target.id).flatten().getItem();
        // get status
        status = await context.model('Thesis')
            .where('id').equal(studentThesis.thesis)
            .select('status/alternateName')
            .silent()
            .value();
        // throw error of thesis status is either completed or cancelled
        if (status === 'completed' || status === 'cancelled') {
            throw new DataError('E_THESIS_STATUS', context.__('Invalid thesis status. Students of a completed or cancelled thesis cannot be modified.'), null, 'Thesis');
        }
        // check if student thesis result is defined

        let studentThesisResults = await context.model('StudentThesisResult').where('thesis').equal(studentThesis.thesis)
            .and('student').equal(studentThesis.student)
            .silent().getItems();
        const graded = studentThesisResults.filter(x => {
            return x.grade != null;
        });
        if (graded.length > 0) {
            throw new DataError('E_THESIS_STATUS', context.__('Cannot remove student. Students with grades cannot be removed.'), null, 'Thesis');
        }
        // delete student thesis results
        await context.model('StudentThesisResult').remove(studentThesisResults);

    }
}
