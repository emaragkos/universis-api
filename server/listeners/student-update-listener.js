import _ from "lodash";
import moment from "moment";
import {DataModel} from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
    const model = event.model;
    const getReferenceMappings = DataModel.prototype.getReferenceMappings;
    model.getReferenceMappings = async function () {
        const res = await getReferenceMappings.bind(this)();
        // remove readonly model CourseExamStudentGrades from mapping before delete
        const mappings = ['CourseExamStudentGrade','StudentAvailableClass','StudentSpecialty'];
        return res.filter((mapping) => {
            return mappings.indexOf(mapping.childModel) < 0;
        });
    };
}
/**
 * @param {DataEventArgs} event
 * @returns {Promise<void>}
 */
async function beforeSaveAsync(event) {

    const context = event.model.context;
    let calculateIdentifiers = false;
    // check if student status is changed from candidate to active
    if (event.state === 2) {
        // get previous state
        const previousStudentStatus = event.previous.studentStatus;
        if (previousStudentStatus.alternateName === 'candidate' && event.target.studentStatus && event.target.studentStatus.alternateName === 'active') {
            // calculate student identifiers
            calculateIdentifiers = true;
        }
        if (event.target.studentStatus && event.target.studentStatus.alternateName !== previousStudentStatus.alternateName) {
            // get active student counselors and update fields is student status is changed
            const studentCounselors = await context.model('StudentCounselor')
                .where('student').equal(event.target.id).and('active').equal(true).silent().getItems();
            if (studentCounselors && studentCounselors.length > 0) {
                // set active to false and update end fields from current year and period
                // update fields
                const student = await  context.model('Student').select('id','department').where('id').equal(event.target.id)
                    .expand('department').silent().getItem();

                for (let i = 0; i < studentCounselors.length; i++) {
                    const studentCounselor = studentCounselors[i];
                    studentCounselor.active = 0;
                    studentCounselor.endDate = new Date();
                    studentCounselor.toPeriod =student.department.currentPeriod;
                    studentCounselor.toYear = student.department.currentYear;
                }
                // save
                await context.model('StudentCounselor').silent().save(studentCounselors);
            }
        }

    }

    if (event.state === 1 || calculateIdentifiers) {
        /**
         * @type {DataContext|*}
         */
        if ((_.isNil(event.target.studentInstituteIdentifier) || _.isNil(event.target.studentIdentifier)) || calculateIdentifiers)
        {
        // get department to calculate student identifiers
        const department = await context.model('LocalDepartment').where('id').equal(event.target.department)
            .select('id','studentIdentifierIndex','studentIdentifierFormat','studentInstituteIdentifierFormat','studentInstituteIdentifierIndex', 'departmentConfiguration').expand('departmentConfiguration').getItem();
        const inscriptionYear = _.isObject(event.target.inscriptionYear)? event.target.inscriptionYear.id : event.target.inscriptionYear;
            if (_.isNil(event.target.studentIdentifier) || calculateIdentifiers) {
                if (department.studentIdentifierFormat) {
                    event.target.studentIdentifier = calculateIdentifier(department.studentIdentifierFormat, department.studentIdentifierIndex, inscriptionYear);
                    department.studentIdentifierIndex = department.studentIdentifierIndex + 1;
                }
            }
            if (_.isNil(event.target.studentInstituteIdentifier) || calculateIdentifiers) {
                if (department.studentIdentifierFormat) {
                    event.target.studentInstituteIdentifier = calculateIdentifier(department.studentInstituteIdentifierFormat, department.studentInstituteIdentifierIndex,inscriptionYear);
                    department.studentInstituteIdentifierIndex = department.studentInstituteIdentifierIndex + 1;
                }
            }
            if (_.isNil(event.target.uniqueIdentifier) || calculateIdentifiers) {
                // get department configuration
                const departmentConfiguration = department.departmentConfiguration;
                if (departmentConfiguration && departmentConfiguration.studentUniqueIdentifierFormat) {
                    // calculate uniqueIdentifier
                    let uniqueIdentifier = calculateIdentifier(departmentConfiguration.studentUniqueIdentifierFormat, departmentConfiguration.studentUniqueIdentifierIndex, inscriptionYear);
                    // ensure uniqueness
                    let exists = await context.model('Student').where('uniqueIdentifier').equal(uniqueIdentifier).silent().count();
                    while (exists > 0) {
                        uniqueIdentifier = calculateIdentifier(departmentConfiguration.studentUniqueIdentifierFormat, departmentConfiguration.studentUniqueIdentifierIndex, inscriptionYear);
                        exists = await context.model('Student').where('uniqueIdentifier').equal(uniqueIdentifier).silent().count();
                    }
                    event.target.uniqueIdentifier = uniqueIdentifier;
                    if (departmentConfiguration.studentUniqueIdentifierIndex != null) {
                        departmentConfiguration.studentUniqueIdentifierIndex = departmentConfiguration.studentUniqueIdentifierIndex + 1;
                    }
                }
            }
            if (department.departmentConfiguration == null || (department.departmentConfiguration && department.departmentConfiguration.studentUniqueIdentifierIndex == null)) {
                // no need to update department configuration
                delete department.departmentConfiguration;
            } 
            await context.model('LocalDepartment').silent().save(department);
        }
    }

    // TODO: calculate student current semester and inscription retro year and period
    if (event.state === 1)
    {
        event.target.inscriptionRetroYear = event.target.inscriptionYear;
        event.target.inscriptionRetroPeriod = event.target.inscriptionPeriod;
     }

    // set specialtyId from studyProgramSpecialty
    if (!_.isNil(event.target.studyProgramSpecialty)) {
        const studyProgramSpecialty =  _.isObject(event.target.studyProgramSpecialty)? event.target.studyProgramSpecialty.id : event.target.studyProgramSpecialty;
        event.target.specialtyId= await context.model('StudyProgramSpecialty').where('id').equal(studyProgramSpecialty).select('specialty').value();
    }

}

function calculateIdentifier (studentIdentifierFormat, studentIdentifierIndex, inscriptionYear) {
    let studentIdentifier;
    studentIdentifier = studentIdentifierFormat;
    let format = studentIdentifierFormat.split(';');
    for (let i = 0; i < format.length; i++) {
        const formatElement = format[i];
        if (formatElement.startsWith('I')) {
            // index with leading zeros
            studentIdentifier = studentIdentifier.replace(formatElement, zeroPad((studentIdentifierIndex + 1), formatElement.length - 1));
        }
        if (formatElement.startsWith('T')) {
            // text
            studentIdentifier = studentIdentifier.replace(formatElement, formatElement.substr(1));
        }
        if (formatElement.startsWith('Y')) {
            // inscription year
            let today = new Date();
            today.setFullYear(inscriptionYear, 1, 1, 0);
            studentIdentifier = studentIdentifier.replace(formatElement, moment(today).format(formatElement));
        }
        if (formatElement.startsWith('R')) {
            const length = parseInt(formatElement.replace(/\D/g,'')); // e.g. gets 11 from 'R11'
            const zeros = zeroPad(0, length - 1).toString();
            const start = parseInt('1' + zeros);
            const end = parseInt('9' + zeros);
            const replacer = Math.floor(start + Math.random() * end);
            studentIdentifier = studentIdentifier.replace(formatElement, replacer.toString());
        }
        if (formatElement.startsWith('M')) {
            // get crc coefficient
            const modulo = parseInt(formatElement.replace(/\D/g,''));
            // remove format element from student identifier
            const currentStudentIdentifier = studentIdentifier.replace(formatElement, '');
            // calculate crc
            const crc = parseInt(currentStudentIdentifier.replace(';','')) % modulo;
            // append to studentIdentifier
            studentIdentifier = studentIdentifier.replace(formatElement, crc.toString());
        }
    }
    return studentIdentifier.replace(/;/g, '');
}
function zeroPad(num, places) {
    let zero = places - num.toString().length + 1;

    return Array(+(zero > 0 && zero)).join("0") + num;
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
