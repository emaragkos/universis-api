import app from '../server/app';
import request from 'supertest';
import { ExpressDataApplication } from '@themost/express';
import { OAuth2ClientService } from '../dist/server/services/oauth2-client-service';
import path from 'path';

describe('Attachment', () => {
    it('should upload attachment', async () => {
    
        /**
         * @type {OAuth2ClientService}
         */
        const service = app.get(ExpressDataApplication.name).getService(OAuth2ClientService);
        const token = await service.authorize({
            client_id: service.settings.client_id,
            client_secret: service.settings.client_secret,
            username: 'registrar1@example.com',
            password: '293EC246',
            grant_type: 'password',
            scope: 'registrar'
        });
        let response = await request(app)
            .get('/api/students/?$filter=studentStatus/alternateName eq \'active\' and semester eq 2&$top=1')
            .set('Authorization', `Bearer ${token.access_token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(200);
        expect(response.body.value).toBeInstanceOf(Array);
        const student = response.body.value[0];
        expect(student).toBeTruthy();

        response = await request(app)
            .get(`/api/students/${student.id}/attachments`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(200);
        expect(response.body.value).toBeInstanceOf(Array);

        response = await request(app)
            .get(`/api/attachmentTypes?$filter=alternateName eq 'test'`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(200);
        expect(response.body.value).toBeInstanceOf(Array);
        expect(response.body.value.length).toBeGreaterThan(0);
        const attachmentType = response.body.value[0];

        response = await request(app)
            .post(`/api/students/${student.id}/attachments/add`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .field('file[attachmentType]', attachmentType.id)
            .attach('file',path.resolve(__dirname, './files/LoremIpsum.pdf'));
        expect(response.statusCode).toBe(200);
        const attachment = response.body;
        expect(attachment).toBeTruthy();

        response = await request(app)
            .get(`${attachment.url}`)
            .set('Authorization', `Bearer ${token.access_token}`)
        expect(response.body).toBeTruthy();

        response = await request(app)
            .post(`/api/students/${student.id}/attachments/${attachment.id}/remove`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .send();
        expect(response.statusCode).toBe(200);        
    });

    fit('should upload attachment to candidate', async () => {
    
        /**
         * @type {OAuth2ClientService}
         */
        const service = app.get(ExpressDataApplication.name).getService(OAuth2ClientService);
        const token = await service.authorize({
            client_id: service.settings.client_id,
            client_secret: service.settings.client_secret,
            username: 'registrar1@example.com',
            password: '293EC246',
            grant_type: 'password',
            scope: 'registrar'
        });
        let response = await request(app)
            .get('/api/StudyProgramRegisterActions?$top=1')
            .set('Authorization', `Bearer ${token.access_token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(200);
        expect(response.body.value).toBeInstanceOf(Array);
        const action = response.body.value[0];
        expect(action).toBeTruthy();

        response = await request(app)
            .get(`/api/attachmentTypes?$filter=alternateName eq 'test'`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        expect(response.statusCode).toBe(200);
        expect(response.body.value).toBeInstanceOf(Array);
        expect(response.body.value.length).toBeGreaterThan(0);
        const attachmentType = response.body.value[0];

        response = await request(app)
            .post(`/api/StudyProgramRegisterActions/${action.id}/AddAttachment`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .field('file[attachmentType]', attachmentType.id)
            .attach('file',path.resolve(__dirname, './files/LoremIpsum.pdf'));
        expect(response.statusCode).toBe(200);
        const attachment = response.body;
        expect(attachment).toBeTruthy();

        response = await request(app)
            .get(`${attachment.url}`)
            .set('Authorization', `Bearer ${token.access_token}`)
        expect(response.body).toBeTruthy();

        response = await request(app)
            .post(`/api/StudyProgramRegisterActions/${action.id}/RemoveAttachment`)
            .set('Authorization', `Bearer ${token.access_token}`)
            .send(attachment);
        expect(response.statusCode).toBe(200);        
    });
});
